import React, { useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Layout from "./hoc/layout/layout";

import SigninOidc from "./pages/signin_oidc";
import SignoutOidc from "./pages/signout_oidc";

import HomePage from "./pages/home_page";
import LoginPage from "./pages/login_page";
import OrderPage from "./pages/orders_page";

import { Provider } from "react-redux";
import store from "./store";
import userManager, { loadUserFromStorage } from "./services/userService";

import AuthProvider from "./utils/auth_provider";
import PrivateRoute from "./utils/protected_route";

function App() {
  useEffect(() => {
    // Получаем пользователя из cookies
    loadUserFromStorage(store);
  }, []);

  return (
    <Provider store={store}>
      <AuthProvider userManager={userManager} store={store}>
        <Router>
          <Layout>
            <Switch>
              <Route exact path="/" component={HomePage} />
              <Route path="/login" component={LoginPage} />
              <Route path="/signout-oidc" component={SignoutOidc} />
              <Route path="/signin-oidc" component={SigninOidc} />
              <PrivateRoute path="/orders" component={OrderPage} />
            </Switch>
          </Layout>
        </Router>
      </AuthProvider>
    </Provider>
  );
}

export default App;
