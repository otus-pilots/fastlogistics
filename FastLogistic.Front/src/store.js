import { createStore, compose } from "redux";
import rootReducer from "./reducers";

const composeEnhancers =
  typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
    : compose;

const initialState = {};

const store = createStore(rootReducer, initialState, composeEnhancers());

export default store;
