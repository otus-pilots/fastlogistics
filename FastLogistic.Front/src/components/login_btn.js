import React, { Component } from "react";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";

class Login extends Component {
  render() {
    return (
      <Button style={{ color: "white" }} to="/login" component={Link}>
        Войти
      </Button>
    );
  }
}

export default Login;
