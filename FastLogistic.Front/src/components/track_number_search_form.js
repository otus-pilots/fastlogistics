import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import InputBase from "@material-ui/core/InputBase";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";
import Alert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";

import axios from "axios";

const useStyles = makeStyles(() => ({
  root: {
    padding: "5px",
    display: "flex",
    alignItems: "center",
    width: 600,
  },
  input: {
    padding: "10px 15px",
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
}));

export default function TrackNumberSearchForm(props) {
  const classes = useStyles();

  const [track, setTrack] = React.useState({});
  const [open, setOpen] = React.useState(false);

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChangeTrack = (event) => {
    setTrack(event.target.value.trim());
  };

  function handleSubmit(e) {
    e.preventDefault();
    props.orderData(null);
    if (!track) {
      return;
    }
    axios
      .get(`/Order/trackNumber/${track}`)
      .then((response) => {
        props.orderData(response.data);
      })
      .catch(() => {
        handleClick();
      });
  }

  return (
    <Paper component="form" className={classes.root} onSubmit={handleSubmit}>
      <InputBase
        onChange={handleChangeTrack}
        className={classes.input}
        placeholder="Введите трек номер заказа"
        inputProps={{ "aria-label": "Трек номер заказа" }}
      />
      <IconButton
        type="submit"
        className={classes.iconButton}
        aria-label="Поиск"
      >
        <SearchIcon />
      </IconButton>
      <Snackbar
        open={open}
        autoHideDuration={5000}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right",
        }}
      >
        <Alert onClose={handleClose} severity="warning">
          Заказ с таким трек номером не найден
        </Alert>
      </Snackbar>
    </Paper>
  );
}
