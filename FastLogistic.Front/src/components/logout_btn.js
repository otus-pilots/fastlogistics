import React, { Component } from "react";
import Button from "@material-ui/core/Button";

import { signoutRedirect } from "../services/userService";

import { Link } from "react-router-dom";

class Logout extends Component {
  logout = () => {
    signoutRedirect();
  };

  render() {
    return (
      <Button
        style={{ color: "white" }}
        onClick={() => {
          this.logout();
        }}
      >
        Выйти
      </Button>
    );
  }
}

export default Logout;
