import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Grid from "@material-ui/core/Grid";
import Chip from "@material-ui/core/Chip";
import Alert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";

import FitnessCenterIcon from "@material-ui/icons/FitnessCenter";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import LocationOffIcon from "@material-ui/icons/LocationOff";
import EventIcon from "@material-ui/icons/Event";
import LocalAtmIcon from "@material-ui/icons/LocalAtm";
import DirectionsRunIcon from "@material-ui/icons/DirectionsRun";
import AttachMoneyIcon from "@material-ui/icons/AttachMoney";

import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import MoreVertIcon from "@material-ui/icons/MoreVert";

import axios from "axios";
import { HubConnectionBuilder, LogLevel } from "@microsoft/signalr";

const useStyles = makeStyles({
  root: {
    width: "100%",

    maxWidth: "600px",
    minHeight: "250px",
    padding: "15px",
  },
  header: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: "10px",
  },
  items: {
    display: "flex",
    alignItems: "center",
  },
  icon: {
    marginRight: "5px",
  },
  status: {
    marginRight: "10px",
  },
});

export default function OrderCard(props) {
  const classes = useStyles();

  const [connection, setConnection] = useState(null);
  const [openSnackbar, setOpenSnackbar] = React.useState(false);

  const { orderData, visiblOptions } = props;

  const [anchorEl, setAnchorEl] = React.useState(null);
  const menuOpen = Boolean(anchorEl);

  const handleMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const deleteOrder = (id) => {
    axios
      .delete(`/Order/${id}`)
      .then(() => {
        props.operationStatus("success");
      })
      .catch((e) => {
        console.log(e);
        props.operationStatus("error");
      });
  };

  useEffect(() => {
    const connect = new HubConnectionBuilder()
      .withUrl("https://localhost:5001/fl/status")
      .configureLogging(LogLevel.Information)
      .withAutomaticReconnect()
      .build();

    setConnection(connect);
  }, []);

  useEffect(() => {
    if (connection) {
      connection
        .start()
        .then(() => {
          connection.invoke("SubscribeOnOrder", orderData.id);
          connection.on("NotifyOthersStatusChenged", (orderId, newStatusId) => {
            setOpenSnackbar(true);
          });
        })
        .catch((error) => console.log(error));
    }
  }, [connection]);

  const NotifyOthersStatusChenged = (orderId, newStatusId) => {
    connection.invoke("NotifyOthersStatusChenged", orderId, newStatusId);
  };

  const ChangeOrderStatus = (newStatusId) => {
    console.log(status);

    axios
      .post(`/OrderStatus`, {
        OrderId: orderData.id,
        StatusId: newStatusId,
      })
      .then(() => {
        console.log("OrderStatus");
      })
      .catch((e) => {
        console.log(e);
        operationCompleted("error");
      });
  };

  return (
    <Card className={classes.root}>
      <div className={classes.header}>
        <span>ID: {orderData.id}</span>
        <div>
          <Chip
            className={classes.status}
            size="small"
            label={`Статус: ${orderData.status}`}
            color="primary"
          />
          {visiblOptions && (
            <span>
              <IconButton
                size="small"
                aria-label="more"
                aria-controls="card-menu"
                aria-haspopup="true"
                onClick={handleMenuOpen}
              >
                <MoreVertIcon />
              </IconButton>
              <Menu
                id="card-menu"
                anchorEl={anchorEl}
                keepMounted
                open={menuOpen}
                onClose={handleClose}
              >
                {!props.seller && orderData.status === "New" && (
                  <MenuItem
                    onClick={() => NotifyOthersStatusChenged(orderData.id, 2)}
                  >
                    Взять в работу
                  </MenuItem>
                )}
                {!props.seller && orderData.status != "New" && (
                  <MenuItem
                    onClick={() => NotifyOthersStatusChenged(orderData.id, 2)}
                  >
                    Изменить статус
                  </MenuItem>
                )}
                {orderData.status == "Draft" && (
                  <MenuItem onClick={() => ChangeOrderStatus(2)}>
                    Oпубликовать
                  </MenuItem>
                )}

                {props.seller && (
                  <MenuItem onClick={() => deleteOrder(orderData.id)}>
                    Удалить
                  </MenuItem>
                )}
              </Menu>
            </span>
          )}
        </div>
      </div>

      <CardContent>
        <Grid container spacing={2}>
          <Grid item xs={6} className={classes.items}>
            <EventIcon className={classes.icon} /> Доставка:{" "}
            {new Date(orderData.preferredTime).toLocaleString()}
          </Grid>
          <Grid item xs={3} className={classes.items}>
            <FitnessCenterIcon className={classes.icon} /> Вес:{" "}
            {orderData.weight}
          </Grid>
          <Grid item xs={3} className={classes.items}>
            <AttachMoneyIcon className={classes.icon} /> Стоимость:{" "}
            {orderData.preferredCost}
          </Grid>
          <Grid item xs={6} className={classes.items}>
            <LocalAtmIcon className={classes.icon} /> Продавец:{" "}
            {orderData.seller ? orderData.seller.name : "не указан"}
          </Grid>
          <Grid item xs={6} className={classes.items}>
            <DirectionsRunIcon className={classes.icon} /> Курьер:{" "}
            {orderData.courier ? orderData.courier : "не назначен"}
          </Grid>
          <Grid item xs={6} className={classes.items}>
            <LocationOnIcon className={classes.icon} /> От:{" "}
            {orderData.addressFrom.registrationAddress}
          </Grid>
          <Grid item xs={6} className={classes.items}>
            <LocationOffIcon className={classes.icon} /> До:{" "}
            {orderData.addressTo.registrationAddress}
          </Grid>
        </Grid>
      </CardContent>
      <Snackbar
        open={openSnackbar}
        autoHideDuration={5000}
        onClose={() => setOpenSnackbar(false)}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right",
        }}
      >
        <Alert onClose={() => setOpenSnackbar(false)} severity="info">
          Cтатус заказа c ID: {orderData.id} изменился
        </Alert>
      </Snackbar>
    </Card>
  );
}
