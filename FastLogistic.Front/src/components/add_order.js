import React, { useEffect } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";

import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

import CustomAlert from "./custom_alert";

import { makeStyles } from "@material-ui/core/styles";

import axios from "axios";

const useStyles = makeStyles((theme) => ({}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function AddOrder(props) {
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);

  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [alertType, setAlertType] = React.useState("success");

  const operationCompleted = (type) => {
    setOpenSnackbar(false);

    if (type === "success") {
      setAlertType("success");
    } else {
      setAlertType("error");
    }

    setOpenSnackbar(true);
  };

  const [weight, setWeight] = React.useState("");
  const [preferredCost, setPreferredCost] = React.useState("");
  const [preferredTime, setPreferredTime] = React.useState("2021-01-01T00:00");
  const [track, setTrack] = React.useState("");
  const [addressFrom, setAddressFrom] = React.useState(null);
  const [addressTo, setAddressTo] = React.useState(null);
  const [draft, setDraft] = React.useState(false);
  const [adressList, setAdressList] = React.useState([]);
  const [validationError, setValidationError] = React.useState([]);

  useEffect(() => {
    if (open) {
      axios
        .get(`/Address`)
        .then((response) => {
          setAdressList(response.data);
        })
        .catch((e) => {
          console.log(e);
        });
    }
  }, [open]);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);

    setWeight("");
    setPreferredTime("2021-01-01T00:00");
    setPreferredCost("");
    setTrack("");
    setAddressFrom(null);
    setAddressTo(null);
    setDraft(false);
    setValidationError([]);
  };

  const validation = () => {
    let errorList = [];
    if (!weight) {
      errorList.push("weight");
    }
    if (!preferredCost) {
      errorList.push("preferredCost");
    }
    if (!track) {
      errorList.push("track");
    }
    if (!addressFrom) {
      errorList.push("addressFrom");
    }
    if (!addressTo) {
      errorList.push("addressTo");
    }
    setValidationError(errorList);
    return Boolean(!errorList.length);
  };

  const handleSubmit = () => {
    if (validation()) {
      axios
        .post(`/Order`, {
          Weight: weight,
          PreferredTime: preferredTime,
          PreferredCost: preferredCost,
          TrackNumber: track,
          AddressFromId: addressFrom.id,
          AddressToId: addressTo.id,
          SellerId: props.seller,
          StatusId: draft ? 1 : 2,
        })
        .then(() => {
          operationCompleted("success");
          handleClose();
          props.fetchOrders();
        })
        .catch((e) => {
          console.log(e);
          operationCompleted("error");
        });
    }
  };

  return (
    <div>
      <Button
        onClick={handleClickOpen}
        variant="contained"
        color="primary"
        disableElevation
      >
        Добавить заказ
      </Button>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">Добавить заказ</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            <Grid container spacing={3}>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  error={validationError.includes("weight")}
                  helperText={
                    validationError.includes("weight")
                      ? "Это обязательное поле"
                      : ""
                  }
                  id="weight"
                  name="weight"
                  label="Вес"
                  fullWidth
                  value={weight}
                  onChange={(event) => {
                    setWeight(event.target.value.trim().replace(/[^0-9]/g, ""));
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  error={validationError.includes("preferredCost")}
                  helperText={
                    validationError.includes("preferredCost")
                      ? "Это обязательное поле"
                      : ""
                  }
                  id="preferredCost"
                  name="preferredCost"
                  label="Цена"
                  fullWidth
                  value={preferredCost}
                  onChange={(event) => {
                    setPreferredCost(
                      event.target.value.trim().replace(/[^0-9]/g, "")
                    );
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  id="preferredTime"
                  label="Время доставки"
                  type="datetime-local"
                  fullWidth
                  InputLabelProps={{
                    shrink: true,
                  }}
                  value={preferredTime}
                  onChange={(event) => {
                    setPreferredTime(event.target.value);
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  error={validationError.includes("track")}
                  helperText={
                    validationError.includes("track")
                      ? "Это обязательное поле"
                      : ""
                  }
                  id="track"
                  name="track"
                  label="Трэк номер"
                  fullWidth
                  value={track}
                  onChange={(event) => {
                    setTrack(event.target.value.trim().replace(/[^0-9]/g, ""));
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <Autocomplete
                  id="addressFrom"
                  noOptionsText="Совпадений нет"
                  options={adressList}
                  getOptionLabel={(option) =>
                    option.registrationAddress ? option.registrationAddress : ""
                  }
                  getOptionSelected={(option, value) =>
                    option.value === value.value
                  }
                  value={addressFrom}
                  onChange={(event, value) => {
                    setAddressFrom(value);
                  }}
                  renderInput={(params) => (
                    <TextField
                      required
                      error={validationError.includes("addressFrom")}
                      helperText={
                        validationError.includes("addressFrom")
                          ? "Это обязательное поле"
                          : ""
                      }
                      {...params}
                      label="Доставка от"
                      value={addressFrom}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12}>
                <Autocomplete
                  id="addressTo"
                  noOptionsText="Совпадений нет"
                  options={adressList}
                  getOptionLabel={(option) =>
                    option.registrationAddress ? option.registrationAddress : ""
                  }
                  getOptionSelected={(option, value) =>
                    option.value === value.value
                  }
                  value={addressTo}
                  onChange={(event, value) => {
                    setAddressTo(value);
                  }}
                  renderInput={(params) => (
                    <TextField
                      required
                      error={validationError.includes("addressTo")}
                      helperText={
                        validationError.includes("addressTo")
                          ? "Это обязательное поле"
                          : ""
                      }
                      {...params}
                      label="Доставка до"
                      value={addressTo}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12}>
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={draft}
                      onChange={(event) => {
                        setDraft(event.target.checked);
                      }}
                      color="primary"
                      name="saveAsDraft"
                    />
                  }
                  label="Сохранить как черновик"
                />
              </Grid>
            </Grid>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleSubmit} color="primary">
            Добавить
          </Button>
          <Button onClick={handleClose} color="secondary">
            Отмена
          </Button>
        </DialogActions>
      </Dialog>
      <CustomAlert open={openSnackbar} type={alertType} />
    </div>
  );
}
