import React, { useEffect } from "react";

import Alert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";

export default function CustomAlert(props) {
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [alertData, setAlertData] = React.useState({
    type: "success",
    text: "Операция успешна",
  });

  useEffect(() => {
    if (props.open) {
      let type = props.type;
      let text;
      if (props.type === "success") {
        text = "Операция успешна";
      } else {
        text = "Произошла ошибка операции";
      }
      setAlertData({
        type,
        text,
      });
      setOpenSnackbar(true);
    }
  }, [props.open]);

  return (
    <Snackbar
      open={openSnackbar}
      autoHideDuration={5000}
      onClose={() => setOpenSnackbar(false)}
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "right",
      }}
    >
      <Alert onClose={() => setOpenSnackbar(false)} severity={alertData.type}>
        {alertData.text}
      </Alert>
    </Snackbar>
  );
}
