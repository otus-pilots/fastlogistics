import React from "react";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { signinRedirect } from "../services/userService";
import { Redirect } from "react-router-dom";
import { useSelector } from "react-redux";

function Login() {
  const user = useSelector((state) => state.auth.user);

  function login() {
    signinRedirect();
  }

  return user ? (
    <Redirect to={"/"} />
  ) : (
    <div style={{ textAlign: "center" }}>
      <Typography
        style={{ padding: "30px" }}
        component="h5"
        variant="h5"
        color="inherit"
        noWrap
      >
        Авторизация
      </Typography>
      <Button variant="contained" color="primary" onClick={() => login()}>
        Войти через IS4
      </Button>
    </div>
  );
}

export default Login;
