import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";

import { withStyles } from "@material-ui/core/styles";

import TrackNumberSearchForm from "../components/track_number_search_form";
import OrderCard from "../components/order_card";

const useStyles = () => ({
  root: {
    marginTop: "200px",
  },
});

class MainPage extends Component {
  state = {
    orderData: null,
  };

  setOrderData = (data) => {
    this.setState({
      orderData: data,
    });
  };

  render() {
    const { classes } = this.props;

    return (
      <Grid
        container
        direction="column"
        alignItems="center"
        spacing={6}
        className={classes.root}
      >
        <Grid item xs={12}>
          <TrackNumberSearchForm orderData={this.setOrderData} />
        </Grid>
        {this.state.orderData && (
          <Grid item xs={12}>
            <OrderCard orderData={this.state.orderData} />
          </Grid>
        )}
      </Grid>
    );
  }
}

export default withStyles(useStyles)(MainPage);
