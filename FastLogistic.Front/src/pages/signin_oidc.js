import React, { useEffect } from "react";

import Typography from "@material-ui/core/Typography";

import { signinRedirectCallback } from "../services/userService";
import { useHistory } from "react-router-dom";

function SigninOidc() {
  const history = useHistory();
  useEffect(() => {
    async function signinAsync() {
      await signinRedirectCallback();
      history.push("/");
    }
    signinAsync();
  }, [history]);

  return (
    <Typography style={{ textAlign: "center" }} component="h5" variant="h5" color="inherit" noWrap>
      Редирект...
    </Typography>
  );
}

export default SigninOidc;
