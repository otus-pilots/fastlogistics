import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";

import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";

import Select from "@material-ui/core/Select";

import AddOrder from "../components/add_order";
import OrderCard from "../components/order_card";
import CustomAlert from "../components/custom_alert";

import axios from "axios";
import jwt_decode from "jwt-decode";

import { useSelector } from "react-redux";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
  card: {
    marginRight: "10px",
    height: "100%",
  },
  empty: {
    margin: "50px auto 0",
    fontSize: "20px",
    fontWeight: 200,
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
  },
  control: {
    marginBottom: "25px",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "baseline",
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
}));

export default function Orders() {
  const classes = useStyles();

  const user = useSelector((state) => state.auth.user);

  const [sellerId, setSellerId] = React.useState(null);
  const [courierId, setCourierId] = React.useState(null);

  const [orders, setOrders] = React.useState([]);
  const [statuses, setStatuses] = React.useState([]);
  const [loading, setLoading] = React.useState(false);

  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [alertType, setAlertType] = React.useState("success");

  const [statusFilter, setStatusFilter] = React.useState("");

  const handleChangeStatusFilter = (event) => {
    setStatusFilter(event.target.value);
  };

  const operationCompleted = (type) => {
    setOpenSnackbar(false);

    if (type === "success") {
      fetchData();
      setAlertType("success");
    } else {
      setAlertType("error");
    }

    setOpenSnackbar(true);
  };

  useEffect(() => {
    fetchUserData();
    fetchStatusList();
    fetchData();
  }, []);

  useEffect(() => {
    fetchData();
  }, [statusFilter]);

  const fetchData = () => {
    setLoading(true);

    let filters = {
      StatusId: statusFilter ? statusFilter : null,
      SellerId: sellerId ? sellerId : null,
    };

    axios
      .post(`/Orders`, filters)
      .then((response) => {
        setOrders(response.data);
      })
      .catch((e) => {
        setOrders([]);
        console.log(e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const fetchStatusList = () => {
    setLoading(true);
    axios
      .get(`/Status`)
      .then((response) => {
        setStatuses(response.data);
      })
      .catch((e) => {
        console.log(e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const fetchUserData = () => {
    setLoading(true);

    let userId;

    if (user) {
      const decodeToken = jwt_decode(user.access_token);
      userId = decodeToken.sub;
    }

    axios
      .get(`/User/${userId}`)
      .then((response) => {
        const { sellerId, courierId } = response.data;
        if (sellerId) {
          setSellerId(sellerId);
        } else if (courierId) {
          setCourierId(courierId);
        }
      })
      .catch((e) => {
        console.log(e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  function statusList() {
    return statuses.map((status) => (
      <MenuItem key={status.id} value={status.id}>
        {status.name}
      </MenuItem>
    ));
  }

  function orderCards() {
    return orders.map((order) => (
      <Grid item xs={6} key={order.id}>
        <OrderCard
          className={classes.card}
          seller={sellerId}
          visiblOptions={true}
          orderData={order}
          operationStatus={(status) => operationCompleted(status)}
        />
      </Grid>
    ));
  }

  return (
    <div className={classes.root}>
      <div className={classes.control}>
        {sellerId && (
          <AddOrder seller={sellerId} fetchOrders={() => fetchData()} />
        )}
        <FormControl className={classes.formControl}>
          <InputLabel id="select-helper-label">Статус</InputLabel>
          <Select
            labelId="select-helper-label"
            id="select-helper"
            value={statusFilter}
            onChange={handleChangeStatusFilter}
          >
            <MenuItem value="">
              <em>All</em>
            </MenuItem>
            {statusList()}
          </Select>
        </FormControl>
      </div>
      <Grid container spacing={2}>
        {orders.length > 0 ? (
          orderCards()
        ) : (
          <Typography
            component="p"
            color="inherit"
            noWrap
            className={classes.empty}
          >
            Данные отсутствуют
          </Typography>
        )}
      </Grid>
      <CustomAlert type={alertType} open={openSnackbar} />
      <Backdrop className={classes.backdrop} open={loading}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </div>
  );
}
