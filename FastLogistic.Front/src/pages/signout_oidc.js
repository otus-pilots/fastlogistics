import React, { useEffect } from "react";

import Typography from "@material-ui/core/Typography";

import { signoutRedirectCallback } from "../services/userService";
import { useHistory } from "react-router-dom";

function SignoutOidc() {
  const history = useHistory();
  useEffect(() => {
    async function signoutAsync() {
      await signoutRedirectCallback();
      history.push("/login");
    }
    signoutAsync();
  }, [history]);

  return (
    <Typography style={{ textAlign: "center" }} component="h5" variant="h5" color="inherit" noWrap>
      Редирект...
    </Typography>
  );
}

export default SignoutOidc;
