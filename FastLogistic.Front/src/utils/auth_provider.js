import React, { useEffect, useRef } from 'react';
import { storeUser } from '../actions/auth_action'
import { setAuthHeader } from './axios_headers';

export default function AuthProvider({ userManager: manager, store, children }) {

  let userManager = useRef();

  useEffect(() => {
    userManager.current = manager

    const onUserLoaded = (user) => {
      console.log(`Пользователь загружен: ${user}`)
      store.dispatch(storeUser(user))
    }

    const onUserUnloaded = () => {
      setAuthHeader(null)
      console.log(`Пользователь выгружен`)
    }

    const onAccessTokenExpiring = () => {
      console.log(`Cрок действия токена пользователя истекает`)
    }

    const onAccessTokenExpired = () => {
      console.log(`Cрок действия токена пользователя истек`)
    }

    const onUserSignedOut = () => {
      console.log(`Пользователь вышел из системы`)
    }

    // События пользователя
    userManager.current.events.addUserLoaded(onUserLoaded)
    userManager.current.events.addUserUnloaded(onUserUnloaded)
    userManager.current.events.addAccessTokenExpiring(onAccessTokenExpiring)
    userManager.current.events.addAccessTokenExpired(onAccessTokenExpired)
    userManager.current.events.addUserSignedOut(onUserSignedOut)

    // Очистка данных
    return function cleanup() {
      userManager.current.events.removeUserLoaded(onUserLoaded);
      userManager.current.events.removeUserUnloaded(onUserUnloaded);
      userManager.current.events.removeAccessTokenExpiring(onAccessTokenExpiring)
      userManager.current.events.removeAccessTokenExpired(onAccessTokenExpired)
      userManager.current.events.removeUserSignedOut(onUserSignedOut)
    };
  }, [manager, store]);

  return (
    React.Children.only(children)
  )
}