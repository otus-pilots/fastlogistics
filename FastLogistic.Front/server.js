const express = require("express");
const path = require("path");
const app = express();
const { createProxyMiddleware } = require("http-proxy-middleware");

app.use(express.static(path.join(__dirname, "build")));

app.get("/", function (req, res) {
  res.sendFile(path.join(__dirname, "build", "index.html"));
});

app.use(
  "/",
  createProxyMiddleware({
    target: "https://fastlogistic:443",
    changeOrigin: true,
    secure: false
  })
);

app.listen(process.env.PORT || 3000, () => {
  console.log("Server start: http://localhost:3000");
});
