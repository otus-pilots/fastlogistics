# FastLogistic Front

## Установка зависимостей
```
npm install
```

### Режим разработки "hot-reloads"
```
npm run start
```

### Сборка и минификация для production
```
npm run build
```