﻿using EventBusInfrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinesEvents
{
    public class UserSMSNotifEvent : IntegrationEvent, IDisposable
    {
        /// <summary>
        /// Номер телефона получателя.
        /// </summary>
        public string PhoneNumber { get; }

        /// <summary>
        /// Тело сообщения.
        /// </summary>
        public string Body { get; set; }


        private bool disposed = false;

        protected UserSMSNotifEvent(string phoneNumber)
        {
            PhoneNumber = phoneNumber;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                }
                disposed = true;
            }
        }

    }
}
