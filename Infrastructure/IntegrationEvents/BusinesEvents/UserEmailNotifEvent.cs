﻿using EventBusInfrastructure;
using System;

namespace BusinesEvents
{
    public abstract class UserEmailNotifEvent : IntegrationEvent, IDisposable
    {
        /// <summary>
        /// Адрес получателя.
        /// </summary>
        public string Email { get; }

        /// <summary>
        /// Заголовок сообщения.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Тело сообщения.
        /// </summary>
        public string Body { get; set; }


        private bool disposed = false;

        protected UserEmailNotifEvent(string email)
        {
            Email = email;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                }
                disposed = true;
            }
        }
    }
}
