﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinesEvents
{
    public class OrderStatusEmailNotifEvent : UserEmailNotifEvent
    {
        private bool IsDisposed = false;

        public OrderStatusEmailNotifEvent(string email, Int64 trackNumber, String status
            )
            : base(email)
        {
            Title = "Статус заказа изменён!";
            Body = "Уважаемы пользователь!"
                    + Environment.NewLine
                    + $"Статус закакза {trackNumber} изменён на {status} ";
        }

        protected override void Dispose(bool disposing)
        {
            if (IsDisposed) return;

            if (disposing)
            {
            }

            IsDisposed = true;
            base.Dispose(disposing);
        }

    }
}
