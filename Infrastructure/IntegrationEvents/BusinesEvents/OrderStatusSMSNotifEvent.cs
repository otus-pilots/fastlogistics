﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinesEvents
{
    public class OrderStatusSMSNotifEvent : UserSMSNotifEvent
    {
        private bool IsDisposed = false;

        public OrderStatusSMSNotifEvent(string phoneNumber, Int64 trackNumber, String status
            )
            : base(phoneNumber )
        {
            Body = "Уважаемы пользователь!"
                    + Environment.NewLine
                    + $"Статус закакза {trackNumber} изменён на {status} ";
        }

        protected override void Dispose(bool disposing)
        {
            if (IsDisposed) return;

            if (disposing)
            {
            }

            IsDisposed = true;
            base.Dispose(disposing);
        }

    }
}
