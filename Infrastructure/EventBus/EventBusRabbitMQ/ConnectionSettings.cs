﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventBusRabbitMQ
{
    /// <summary>
    /// Настройки подключения к RabbitMQ.
    /// </summary>
    public class ConnectionSettings
    {

        /// <summary>
        /// Имя секции конфигурации с настройками подключения к RabbitMQ.
        /// </summary>
        public const String SectionName = "EventBus";

        /// <summary>
        /// Адрес подключения.
        /// </summary>
        public String EventBusConnection { get; set; }

        /// <summary>
        /// Логин.
        /// </summary>
        public String EventBusUserName { get; set; }

        /// <summary>
        /// Пароль.
        /// </summary>
        public String EventBusPassword { get; set; }

        /// <summary>
        /// Виртуальный хост.
        /// </summary>
        public String EventBusVirtualHost { get; set; }

        /// <summary>
        /// Количество попыток подключения.
        /// </summary>
        public String EventBusRetryCount { get; set; }

        public String SubscriptionClientName { get; set; }
    }
}
