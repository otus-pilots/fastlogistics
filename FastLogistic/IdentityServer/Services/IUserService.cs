﻿using IdentityServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Services
{
    public interface IUserService
    {
        /// <summary>
        /// Возвращает пароль о пользователе по логину.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        Task<UserShortResponse> GetUserCredByLoginAsync(string login);

        /// <summary>
        /// Возвращает информацию о пользователе по логину.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        Task<UserResponse> GetUserByLoginAsync(string login);

        Task<UserResponse> GetUserByEmailAsync(string email);
    }
}
