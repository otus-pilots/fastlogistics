﻿using IdentityServer.Models;
using RestSharp;
using RestSharp.Serializers.NewtonsoftJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace IdentityServer.Services
{
    internal sealed class UserClient 
    {
        private readonly RestClient _rest;
        private readonly String _url;
        private readonly String path = "/User";

        public UserClient( string url)
        {
            _rest = new RestClient(url) ;
            _rest.UseNewtonsoftJson();
            _url = url ;

            //Так нельзя делать в проде. Только для разоаботки.
            _rest.RemoteCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
        }

        public async Task<IRestResponse> CreateUser(UserResponse model, CancellationToken ct = default)
        {
            var uri = new Uri($"{_url}{path}");
            var request = new RestRequest(uri, Method.POST);
            request.AddJsonBody(MapToAddUser(model));
            return await _rest.ExecuteAsync(request, ct).ConfigureAwait(false);
        }

        private AddUser MapToAddUser (UserResponse model)
        {
            return new AddUser()
            {
                Email = model.Email,
                Login = model.Login,
                Password = model.Email
            };
        }
    }
}
