﻿using FastLogistic.Core.Abstractions.Repositories;
using IdentityServer.Mappers;
using IdentityServer.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Services.Impl
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly ILogger<UserService> _logger;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="userRepository"></param>
        /// <param name="logger"></param>
        public UserService(
            IUserRepository userRepository,
            ILogger<UserService> logger
            )
        {
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        ///<inheritdoc/>
        public async Task<UserShortResponse> GetUserCredByLoginAsync(String login)
        {
            var user = await _userRepository.GetUserCredByLoginAsync(login);

            if (user is null)
                return null;

            var userModel = UserMapper.MapFromModelToShortResponse(user);

            return userModel;
        }

        ///<inheritdoc/>
        public async Task<UserResponse> GetUserByLoginAsync(String login)
        {
            var user = await _userRepository.GetUserByLoginAsync(login);

            if (user is null)
                return null;

            var userModel = UserMapper.MapFromModelToResponse(user);

            return userModel;
        }

        public async Task<UserResponse> GetUserByEmailAsync(String email)
        {
            var user = await _userRepository.GetUserByEmailAsync(email);

            if (user is null)
                return null;

            var userModel = UserMapper.MapFromModelToResponse(user);

            return userModel;
        }


    }
}
