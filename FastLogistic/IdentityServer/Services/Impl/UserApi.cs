﻿using IdentityServer.Models;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace IdentityServer.Services.Impl
{
    internal class UserApi : IUserApi
    {
        private readonly UserClient _userClient;

        public UserApi(IConfiguration configuration)
        {
            var url = configuration.GetValue<string>("WebApi");
            _userClient = new UserClient(url);
        }

        public async Task<String> CreateUser(UserResponse model)
        {
            var id = await _userClient.CreateUser(model).ConfigureAwait(false);

            return id.Content;
        }
    }
}