﻿using IdentityServer.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Services
{
    public interface IUserApi
    {
        Task<String> CreateUser(UserResponse model);
    }
}
