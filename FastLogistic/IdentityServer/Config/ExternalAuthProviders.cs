﻿using IdentityServer4;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace IdentityServer.Config
{
    public static class ExternalAuthProviders
    {
        public static IServiceCollection AddExternalAuthProviders(this IServiceCollection services,
            IConfiguration configuration)
        {
            var externalConfig = configuration.GetSection(nameof(External)).Get<External>();

            services
                .AddAuthentication()
                .AddGoogle(options =>
                {
                    options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;

                    options.ClientId = externalConfig.Google.ClientId;
                    options.ClientSecret = externalConfig.Google.ClientSecret;
                })
                .AddFacebook(options =>
                {
                    options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;

                    options.AppId = externalConfig.Facebook.AppId;
                    options.AppSecret = externalConfig.Facebook.AppSecret;
                });

            return services;
        }
    }
}