using System.Collections.Generic;
using IdentityModel;
using IdentityServer4.Models;
using Microsoft.Extensions.Configuration;

namespace IdentityServer.Config
{
    public static class IdentityConfig
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new()
                {
                    Name = "roles",
                    DisplayName = "Roles",
                    UserClaims = {JwtClaimTypes.Role}
                }
            };

        public static IEnumerable<ApiResource> ApiResources =>
            new ApiResource[]
            {
                new("api1", "FastLogistic API", new []{JwtClaimTypes.Role}) { Scopes = { "api1" } }
            };

        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            {
                new("api1", "FastLogistic API")
            };

        public static IEnumerable<Client> Clients(IConfiguration configuration)
        {
            var swaggerConfig = configuration.GetSection("SwaggerClient");
            return new Client[]
            {
                new()
                {
                    ClientId = "swagger",
                    ClientName = "Swagger UI",
                    ClientSecrets = { new Secret("pass".Sha256()) },

                    AllowedGrantTypes = GrantTypes.Code,
                    RequirePkce = true,
                    RequireClientSecret = false,

                    RedirectUris = { swaggerConfig.GetSection("RedirectUris").Get<string>() },
                    AllowedCorsOrigins = { swaggerConfig.GetSection("AllowedCorsOrigins").Get<string>() },
                    AllowedScopes = { "api1", "roles" },
                },
                new ()
                {
                    // unique ID for this client
                    ClientId = "UIClient", 
                    // human-friendly name displayed in IS
                    ClientName = "UI Client", 
                    // URL of client
                    ClientUri = configuration.GetSection("UIClient:ClientUri").Get<string>(), 
                    // how client will interact with our identity server (Implicit is basic flow for web apps)
                    AllowedGrantTypes = GrantTypes.Implicit, 
                    // don't require client to send secret to token endpoint
                    RequireClientSecret = false,
                    RedirectUris =
                    {             
                        // can redirect here after login                     
                        configuration.GetSection("UIClient:RedirectUris").Get<string>()
                    },
                    // can redirect here after logout
                    PostLogoutRedirectUris = { configuration.GetSection("UIClient:PostLogoutRedirectUris").Get<string>() },
                    BackChannelLogoutUri = configuration.GetSection("UIClient:PostLogoutRedirectUris").Get<string>(),
                    FrontChannelLogoutUri = configuration.GetSection("UIClient:PostLogoutRedirectUris").Get<string>(),
                    // builds CORS policy for javascript clients
                    AllowedCorsOrigins = { configuration.GetSection("UIClient:AllowedCorsOrigins").Get<string>() }, 
                    // what resources this client can access
                    AllowedScopes = { "openid", "api1", "roles" }, 
                    // client is allowed to receive tokens via browser
                    AllowAccessTokensViaBrowser = true
                }
            };
        }
    }
}