namespace IdentityServer.Config
{
    public class External
    {
        public Google Google { get; set; } = new();
        public Facebook Facebook { get; set; } = new();
    }

    public class Google
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }

    public class Facebook
    {
        public string AppId { get; set; }
        public string AppSecret { get; set; }
    }
}