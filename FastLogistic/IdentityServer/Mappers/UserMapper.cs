﻿using FastLogistic.Core.Domain;
using IdentityServer.Models;

namespace IdentityServer.Mappers
{
    public static class UserMapper
    {
        public static UserShortResponse MapFromModelToShortResponse(User user)
        {
            if (user == null)
            {
                return null;
            }

            return new UserShortResponse
            {
                SubjectId = user.Id.ToString(),
                Login = user.Login,
                Email = user.Email,
                Password = user.Password
            };
        }

        public static UserResponse MapFromModelToResponse(User user)
        {
            return new UserResponse
            {
                SubjectId = user.Id.ToString(),
                Login = user.Login,
                Email = user.Email,
                Password = user.Password,
                Role = user.Role.Name
                //SellerId = user.Seller?.Id,
                //CourierId = user.Courier?.Id,
                //PhoneNumber = user.PhoneNumber
            };
        }



    }
}
