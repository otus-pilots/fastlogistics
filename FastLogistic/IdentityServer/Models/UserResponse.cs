using System;

namespace IdentityServer.Models
{
    public class UserResponse
    {
        public string SubjectId { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public String Role { get; set; }

    }
}