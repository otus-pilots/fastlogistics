﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Models
{
    public class UserShortResponse
    {
        public string SubjectId { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }



    }
}
