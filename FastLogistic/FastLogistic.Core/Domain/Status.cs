﻿using System.Collections.Generic;

namespace FastLogistic.Core.Domain
{
    /// <summary>
    /// Статус доставки заказа
    /// </summary>
    public class Status : BaseEntity
    {
        /// <summary>
        /// Название статуса
        /// </summary>		
        public string Name { get; set; }

        public List<Order> Orders { get; set; } = new List<Order>();
        public List<OrderStatus> OrderStatuses { get; set; } = new List<OrderStatus>();
    }
}