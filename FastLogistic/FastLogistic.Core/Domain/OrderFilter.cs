namespace FastLogistic.Core.Domain
{
    public class OrderFilter
    {
        /// <summary>
        ///     ID курьера
        /// </summary>
        public int? CourierId { get; set; }

        /// <summary>
        ///     ID продавца
        /// </summary>
        public int? SellerId { get; set; }

        /// <summary>
        ///     ID адреса отправки
        /// </summary>
        public int? AddressFromId { get; set; }

        /// <summary>
        ///     ID адреса доставки
        /// </summary>
        public int? AddressToId { get; set; }

        /// <summary>
        ///     ID зоны отправки
        /// </summary>
        public int? ZoneFromId { get; set; }

        /// <summary>
        ///     ID зоны доставки
        /// </summary>
        public int? ZoneToId { get; set; }

        /// <summary>
        ///     ID статуса
        /// </summary>
        public int? StatusId { get; set; }
    }
}