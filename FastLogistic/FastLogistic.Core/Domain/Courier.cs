using System.Collections.Generic;

namespace FastLogistic.Core.Domain
{
    /// <summary>
    ///     Курьер
    /// </summary>
    public class Courier : BaseEntity
    {
        /// <summary>
        ///     Внешний ключ для связи с пользователем
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        ///     Навигационное свойство для связи с пользователем
        /// </summary>
        public User User { get; set; }

        /// <summary>
        ///     Базовый тариф (стоимость) доставки
        /// </summary>
        public double BaseRate { get; set; }

        /// <summary>
        ///     Базовое время (скорость) доставки
        /// </summary>
        public double BaseTime { get; set; }

        /// <summary>
        ///     Максимальный вес, который может доставлять курьер
        /// </summary>
        public double MaxWeight { get; set; }

        /// <summary>
        ///     Коэффициент наценки за вес
        /// </summary>
        public double WeightRate { get; set; }

        /// <summary>
        ///     Навигационное свойство для связи со списком заказов
        /// </summary>
        public List<Order> Orders { get; set; }

        /// <summary>
        ///     Навигационное свойство для связи со списком зон
        /// </summary>
        public List<Zone> Zones { get; set; }

        /// <summary>
        ///     Навигационное свойство для связи с промежуточной сущностью CourierZone
        /// </summary>
        public List<CourierZone> CourierZones { get; set; }
    }
}