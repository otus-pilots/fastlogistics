﻿using System.Collections.Generic;

namespace FastLogistic.Core.Domain
{
    /// <summary>
    ///     Зона доставки курьера
    /// </summary>
    public class Zone : BaseEntity
    {
        /// <summary>
        ///     Название зоны доставки
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Коэффициент стоимости доставки для зоны
        /// </summary>
        public double CostRate { get; set; }

        /// <summary>
        ///     Коэффициент времени доставки для зоны
        /// </summary>
        public double TimeRate { get; set; }

        /// <summary>
        ///     Навигационное свойство для связи с адресами данной зоны
        /// </summary>
        public List<Address> Addresses { get; set; }

        /// <summary>
        ///     Навигационное свойство для связи со списком курьеров
        /// </summary>
        public List<Courier> Couriers { get; set; }

        /// <summary>
        ///     Навигационное свойство для связи с промежуточной сущностью CourierZone
        /// </summary>
        public List<CourierZone> CourierZones { get; set; }
    }
}