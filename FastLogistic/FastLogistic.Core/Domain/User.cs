using System.Collections.Generic;

namespace FastLogistic.Core.Domain
{
    /// <summary>
    ///     Пользователь системы
    /// </summary>
    public class User : BaseEntity
    {
        /// <summary>
        ///     Логин пользователя
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        ///     Пароль или хэш пароля пользователя
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        ///     Электронная почта
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Номер телефона.
        /// </summary>
        public string PhoneNumber { get; set; } = "";
        /// <summary>
        ///     Внешний ключ для связи с ролью
        /// </summary>
        public int RoleId { get; set; }

        /// <summary>
        ///     Навигационное свойство для связи с ролью
        /// </summary>
        public Role Role { get; set; }

        /// <summary>
        ///     Навигационное свойство для связи с продавцом
        /// </summary>
        public Seller Seller { get; set; }

        /// <summary>
        ///     Навигационное свойство для связи с курьером
        /// </summary>
        public Courier Courier { get; set; }

        /// <summary>
        /// Навигационное свойство для связи с заказами.
        /// </summary>
        public List<Order> Orders { get; set; } = new List<Order>();
        public List<UserOrders> UserOrders { get; set; } = new List<UserOrders>();

    }
}