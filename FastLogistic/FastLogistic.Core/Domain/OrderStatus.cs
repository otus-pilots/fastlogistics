﻿using System;

namespace FastLogistic.Core.Domain
{
    public class OrderStatus
    {
        /// <summary>
        ///     Ссылка на заказ.
        /// </summary>
        public int OrderId { get; set; }

        public Order Order { get; set; }

        /// <summary>
        ///     Ссылка на статус.
        /// </summary>
        public int StatusId { get; set; }

        public Status Status { get; set; }

        /// <summary>
        ///     Дата изменения заказа.
        /// </summary>
        public DateTime TimeChanged { get; set; } = DateTime.Now;
    }
}