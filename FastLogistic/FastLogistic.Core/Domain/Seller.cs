using System.Collections.Generic;

namespace FastLogistic.Core.Domain
{
    /// <summary>
    ///     Продавец
    /// </summary>
    public class Seller : BaseEntity
    {
        /// <summary>
        ///     Наименование продавца
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Внешний ключ для связи с пользователем
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        ///     Навигационное свойство для связи с пользователем
        /// </summary>
        public User User { get; set; }

        /// <summary>
        ///     Навигационное свойство для связи с размещёнными заказами
        /// </summary>
        public List<Order> Orders { get; set; }
    }
}