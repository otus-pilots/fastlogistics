﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;

namespace FastLogistic.Core.Domain
{
    public class Order : BaseEntity
    {
        /// <summary>
        ///     Вес заказа
        /// </summary>
        public decimal Weight { get; set; }

        /// <summary>
        ///     Предполагаемое время доставки
        /// </summary>
        public DateTime PreferredTime { get; set; }

        /// <summary>
        ///     Предполагаемая стоимость
        /// </summary>
        public decimal PreferredCost { get; set; }

        /// <summary>
        ///     Трек-номер
        /// </summary>
        public long TrackNumber { get; set; }

        /// <summary>
        ///     Внешний ключ для связи с курьером
        /// </summary>
        public int? CourierId { get; set; }
        
        /// <summary>
        ///     Навигационное свойство для связи с курьером
        /// </summary>
        public Courier Courier { get; set; }

        /// <summary>
        ///     Внешний ключ для связи с продавцом
        /// </summary>
        public int? SellerId { get; set; }
        
        /// <summary>
        ///     Навигационное свойство для связи с продавцом
        /// </summary>
        public Seller Seller { get; set; }

        /// <summary>
        ///     Id адреса отправления
        /// </summary>
        public int AddressFromId { get; set; }

        //Адрес отправления
        public Address AddressFrom { get; set; }

        /// <summary>
        ///     Id адреса назначения
        /// </summary>
        public int AddressToId { get; set; }

        //Адрес назначения
        public Address AddressTo { get; set; }

        private List<Status> _statuses ;
        public List<OrderStatus> OrderStatuses { get; set; } = new List<OrderStatus>();

        /// <summary>
        /// Навигационное свойство для связи с пользователями.
        /// </summary>
        private List<User> _users  ;
        public List<UserOrders> UserOrders { get; set; } = new List<UserOrders>();



        public Order()
        {
        }

        private Order(ILazyLoader lazyLoader)
        {
            LazyLoader = lazyLoader;
        }

        private ILazyLoader LazyLoader { get; set; }

        public List<Status> Statuses
        {
            get => LazyLoader.Load(this, ref _statuses);
            set => _statuses = value;
        }

        public List<User> Users
        {
            get => LazyLoader.Load(this, ref _users);
            set => _users = value;
        }


    }
}