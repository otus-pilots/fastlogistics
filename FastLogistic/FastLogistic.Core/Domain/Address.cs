﻿namespace FastLogistic.Core.Domain
{
    /// <summary>
    ///     Адрес доставки заказа
    /// </summary>
    public class Address : BaseEntity
    {
        /// <summary>
        ///     Адрес заказа
        /// </summary>
        public string RegistrationAddress { get; set; }

        /// <summary>
        ///     Внешний ключ для связи с зоной доставки
        /// </summary>
        public int ZoneId { get; set; }

        /// <summary>
        ///     Навигационное свойство для связи с зоной доставки
        /// </summary>
        public Zone Zone { get; set; }
    }
}