namespace FastLogistic.Core.Domain
{
    public class CourierZone
    {
        /// <summary>
        ///     Внешний ключ для связи с курьером
        /// </summary>
        public int CourierId { get; set; }

        /// <summary>
        ///     Навигационное свойство для связи с курьером
        /// </summary>
        public Courier Courier { get; set; }

        /// <summary>
        ///     Внешний ключ для связи с зоной доставки
        /// </summary>
        public int ZoneId { get; set; }

        /// <summary>
        ///     Навигационное свойство для связи с зоной доставки
        /// </summary>
        public Zone Zone { get; set; }
    }
}