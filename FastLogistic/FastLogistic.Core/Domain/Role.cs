using System.Collections.Generic;

namespace FastLogistic.Core.Domain
{
    /// <summary>
    ///     Роль пользователя
    /// </summary>
    public class Role : BaseEntity
    {
        /// <summary>
        ///     Название роли
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Навигационное свойство для связи с пользователями
        /// </summary>
        public List<User> Users { get; set; }
    }
}