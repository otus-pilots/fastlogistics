using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastLogistic.Core.Domain
{
    /// <summary>
    /// Статические роли.
    /// </summary>
    public enum RoleStatics
    {
        Guest = 1,
        Registered = 2,
        Manager = 3,
        Admin = 4,
        Courier = 5,
        Seller = 6
    }
}
