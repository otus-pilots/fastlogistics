﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastLogistic.Core.Domain
{
    public class UserOrders
    {
        /// <summary>
        ///     Ссылка на заказ.
        /// </summary>
        public int OrderId { get; set; }

        public Order Order { get; set; }

        /// <summary>
        ///     Ссылка на пользователя.
        /// </summary>
        public int UserId { get; set; }

        public User User { get; set; }

        /// <summary>
        /// Маркер активности заказа.
        /// </summary>
        public Boolean IsActive { get; set; } = true;

    }
}
