using FastLogistic.Core.Domain;

namespace FastLogistic.Core.Abstractions.Repositories
{
    public interface ISellerRepository : IRepository<Seller>
    {
    }
}