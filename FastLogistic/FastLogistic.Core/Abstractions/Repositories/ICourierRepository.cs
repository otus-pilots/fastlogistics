using System.Threading.Tasks;
using FastLogistic.Core.Domain;

namespace FastLogistic.Core.Abstractions.Repositories
{
    public interface ICourierRepository : IRepository<Courier>
    {
        /// <summary>
        /// Возвращает курьера по Id пользователя
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<Courier> GetCourierIdByUserIdAsync(int userId);
    }
}