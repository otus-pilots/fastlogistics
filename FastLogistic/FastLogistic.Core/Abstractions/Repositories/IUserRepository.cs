using FastLogistic.Core.Domain;
using System.Threading.Tasks;

namespace FastLogistic.Core.Abstractions.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        /// <summary>
        /// Пользователь с прикреплёнными заказами.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<User> GetUserOrdersByIdAsync(int id);

        /// <summary>
        /// Получение информации о пользователе по логину.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        Task<User> GetUserByLoginAsync(string login);

        /// <summary>
        /// Получение информации о пользователе по логину.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        Task<User> GetUserByEmailAsync(string email);
        
        /// <summary>
        /// Возвращает credentionals по логину.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        Task<User> GetUserCredByLoginAsync(string login);
    }
}