﻿using System.Threading.Tasks;

namespace FastLogistic.Core.Abstractions.Repositories
{
    public interface IDistributedCacheRepository
    {
        /// <summary>
        /// Получает данные из кеша. 
        /// </summary>
        /// <typeparam name="TV">Тип данных</typeparam>
        /// <param name="key">ключ кеша</param>
        /// <returns></returns>
        Task<TV> GetCache<TV>(string key) where TV : class;

        /// <summary>
        /// Помещает данные в кеш.
        /// </summary>
        /// <typeparam name="TV">Тип данных</typeparam>
        /// <param name="key">ключ кеша</param>
        /// <param name="item">Данные</param>
        /// <returns></returns>
        Task SetCache<TV>(string key, TV item) where TV : class;
    }
}
