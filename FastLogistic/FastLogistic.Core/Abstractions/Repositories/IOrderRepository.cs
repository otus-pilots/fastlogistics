using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FastLogistic.Core.Domain;

namespace FastLogistic.Core.Abstractions.Repositories
{
    public interface IOrderRepository : IRepository<Order>
    {
        Task<List<Order>> GetAllFilteredAsync(OrderFilter filter);
        Task<List<OrderStatus>> GetOrderStatusesByIdAsync(int id);

        /// <summary>
        /// Возвращает заказ по трек номеру
        /// </summary>
        /// <param name="trackNumber"></param>
        /// <returns></returns>
        Task<Order> GetOrderByTrackAsync(Int64 trackNumber);

    }
}