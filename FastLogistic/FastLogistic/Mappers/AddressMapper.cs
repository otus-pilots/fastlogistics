﻿using FastLogistic.Core.Domain;
using FastLogistic.Models.Address;

namespace FastLogistic.Mappers
{
    public static class AddressMapper
    {
        public static AddressResponse MapFromModelToResponse(Address address)
        {
            return new AddressResponse
            {
                Id = address.Id,
                RegistrationAddress = address.RegistrationAddress,
                ZoneId = address.ZoneId
            };
        }

        public static Address MapFromAddRequestToModel(AddAddressRequest request)
        {
            return new Address
            {
                RegistrationAddress = request.RegistrationAddress,
                ZoneId = request.ZoneId
            };
        }

        public static void MapFromUpdateRequest(Address address, UpdateAddressRequest request)
        {
            address.RegistrationAddress = request.RegistrationAddress;
            address.ZoneId = request.ZoneId;
        }
    }
}