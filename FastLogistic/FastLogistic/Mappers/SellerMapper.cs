﻿using FastLogistic.Core.Domain;
using FastLogistic.Models.Seller;
using System.Collections.Generic;

namespace FastLogistic.Mappers
{
    public static class SellerMapper
    {
        public static Seller MapFromAddRequestToModel(AddSellerRequest request)
        {
            return new Seller
            {
                Name = request.Name,
                UserId = request.UserId,
            };
        }

        public static void MapFromUpdateRequest(Seller seller, UpdateSellerRequest request)
        {
            seller.Name = request.Name;
        }

        public static SellerShortResponse MapFromModelToShortResponse(Seller seller)
        {
            return new SellerShortResponse()
            {
                Id = seller.Id,
                Name = seller.Name,
            };
        }

        public static SellerResponse MapFromModelToResponse(Seller seller)
        {
            var userShortResponse = UserMapper.MapFromModelToShortResponse(seller.User);

            return new SellerResponse()
            {
                Id = seller.Id,
                Name = seller.Name,
                User = userShortResponse
            };
        }
    }
}
