using FastLogistic.Core.Domain;
using FastLogistic.Models.Statuses;

namespace FastLogistic.Mappers
{
    internal class StatusMapper
    {
        public static StatusResponse MapFromModelToResponse(Status status)
        {
            return new StatusResponse
            {
                Id = status.Id,
                Name = status.Name
            };
        }
    }
}