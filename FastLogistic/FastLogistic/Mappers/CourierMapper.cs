﻿using FastLogistic.Core.Domain;
using FastLogistic.Models.Courier;
using System.Collections.Generic;

namespace FastLogistic.Mappers
{
    public static class CourierMapper
    {
        public static Courier MapFromAddRequestToModel(AddCourierRequest request, List<Zone> zones)
        {
            return new Courier
            {
                BaseRate = request.BaseRate.Value,
                BaseTime = request.BaseTime.Value,
                MaxWeight = request.MaxWeight.Value,
                WeightRate = request.WeightRate.Value,
                UserId = request.UserId.Value,
                Zones = zones
            };
        }

        public static void MapFromUpdateRequest(Courier courier, UpdateCourierRequest request, List<Zone> zones)
        {
            courier.BaseRate = request.BaseRate.Value;
            courier.BaseTime = request.BaseTime.Value;
            courier.MaxWeight = request.MaxWeight.Value;
            courier.WeightRate = request.WeightRate.Value;
            courier.Zones = zones;
        }

        public static CourierShortResponse MapFromModelToShortResponse(Courier courier)
        {
            return new CourierShortResponse()
            {
                Id = courier.Id,
                BaseRate = courier.BaseRate,
                BaseTime = courier.BaseTime,
                MaxWeight = courier.MaxWeight,
                WeightRate = courier.WeightRate,
            };
        }
        
        public static CourierResponse MapFromModelToResponse(Courier courier)
        {
            var userShortResponse = UserMapper.MapFromModelToShortResponse(courier.User);

            return new CourierResponse()
            {
                Id = courier.Id,
                BaseRate = courier.BaseRate,
                BaseTime = courier.BaseTime,
                MaxWeight = courier.MaxWeight,
                WeightRate = courier.WeightRate,
                User = userShortResponse
            };
        }
    }
}
