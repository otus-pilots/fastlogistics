﻿using System.Collections.Generic;
using FastLogistic.Core.Domain;
using FastLogistic.Models.Order;
using System.Linq;

namespace FastLogistic.Mappers
{
    internal class OrderMapper
    {
        public static Order MapFromAddRequestToModel(AddOrderRequest request)
        {
            return new Order
            {
                Weight = request.Weight,
                PreferredCost = request.PreferredCost,
                PreferredTime = request.PreferredTime,
                TrackNumber = request.TrackNumber,
                AddressFromId = request.AddressFromId,
                AddressToId = request.AddressToId,
                SellerId = request.SellerId,
                OrderStatuses = new List<OrderStatus> { new() { StatusId = request.StatusId } }
            };
        }

        public static OrderShortResponse MapFromModelToShortResponse(Order order)
        {
            return new OrderShortResponse
            {
                Id = order.Id,
                Weight = order.Weight,
                CourierId = order.Courier?.Id,
                AddressIdTo = order.AddressToId,
                AddressIdFromId = order.AddressFromId,
                TrackNumber = order.TrackNumber
            };
        }

        public static OrderResponse MapFromModelToResponse(Order order)
        {
            var statuses = order.OrderStatuses;
            return new OrderResponse
            {
                Id = order.Id,
                Weight = order.Weight,
                TrackNumber = order.TrackNumber,
                AddressFrom = order.AddressFrom,
                AddressTo = order.AddressTo,
                PreferredCost = order.PreferredCost,
                PreferredTime = order.PreferredTime,
                Status = order.Statuses.Where(x=> x.Id == statuses.OrderBy(y => y.TimeChanged).Last().StatusId).First().Name,
                Courier = order.Courier,
                Seller = order.Seller
            };
        }

        public static void MapFromUpdateRequest(ref Order order, UpdateOrderRequest request)
        {
            order.Weight = request.Weight;
            order.Weight = request.TrackNumber;
            order.PreferredCost = request.PreferredCost;
            order.PreferredTime = request.PreferredTime;
            order.AddressFromId = request.AddressFromId;
            order.AddressToId = request.AddressToId;
        }

        public static OrderFilter MapFromFilterRequest(GetOrdersFilterRequest request)
        {
            return new OrderFilter
            {
                CourierId = request.CourierId,
                SellerId = request.SellerId,
                AddressFromId = request.AddressFromId,
                AddressToId = request.AddressToId,
                ZoneFromId = request.ZoneFromId,
                ZoneToId = request.ZoneToId,
                StatusId = request.StatusId
            };
        }
    }
}