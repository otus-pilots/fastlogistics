﻿using FastLogistic.Core.Domain;
using FastLogistic.Models.Zone;

namespace FastLogistic.Mappers
{
    public static class ZoneMapper
    {
        public static ZoneResponse MapFromModelToResponse(Zone zone)
        {
            return new ZoneResponse
            {
                Id = zone.Id,
                CostRate = zone.CostRate,
                Name = zone.Name,
                TimeRate = zone.TimeRate
            };
        }

        public static Zone MapFromAddRequestToModel(AddZoneRequest request)
        {
            return new Zone
            {
                CostRate = request.CostRate,
                Name = request.Name,
                TimeRate = request.TimeRate
            };
        }

        public static void MapFromUpdateRequest(ref Zone zone, UpdateZoneRequest request)
        {
            zone.CostRate = request.CostRate;
            zone.Name = request.Name;
            zone.TimeRate = request.TimeRate;
        }
    }
}
