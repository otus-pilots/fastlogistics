﻿using FastLogistic.Core.Domain;
using FastLogistic.Models.User;

namespace FastLogistic.Mappers
{
    public static class UserMapper
    {
        public static UserShortResponse MapFromModelToShortResponse(User user)
        {
            if (user == null)
            {
                return null;
            }

            return new UserShortResponse
            {
                Id = user.Id,
                Login = user.Login,
                Email = user.Email
            };
        }

        public static UserResponse MapFromModelToResponse(User user)
        {
            return new UserResponse
            {
                Id = user.Id,
                Login = user.Login,
                Email = user.Email,
                RoleId = user.RoleId,
                SellerId = user.Seller?.Id,
                CourierId = user.Courier?.Id,
                PhoneNumber = user.PhoneNumber
            };
        }

        public static User MapFromAddRequestToModel(AddUserRequest request)
        {
            return new User
            {
                Login = request.Login,
                Password = request.Password,
                Email = request.Email,
            };
        }

        public static void MapFromUpdateInfoRequest(User user, UpdateUserInfoRequest request)
        {
            user.Login = request.Login;
            user.Email = request.Email;
            user.Password = request.Password;
            user.PhoneNumber = request.PhoneNumber;
        }

        public static void MapFromUpdateRoleRequest(User user, UpdateUserRoleRequest request)
        {
            user.RoleId = request.RoleId;
        }
    }
}
