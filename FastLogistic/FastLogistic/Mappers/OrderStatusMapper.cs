using FastLogistic.Core.Domain;
using FastLogistic.Models.OrderStatus;

namespace FastLogistic.Mappers
{
    internal class OrderStatusMapper
    {
        public static OrderStatusResponse MapFromModelToResponse(OrderStatus orderStatus)
        {
            return new OrderStatusResponse
            {
                StatusName = orderStatus.Status.Name,
                TimeChanged = orderStatus.TimeChanged
            };
        }
    }
}