using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FastLogistic.Models.Statuses;
using FastLogistic.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FastLogistic.Controllers
{
    /// <summary>
    ///     Типы статусов заказов
    /// </summary>
    [Route("[controller]")]
    [ApiController]
    public class StatusController : ControllerBase
    {
        private readonly IStatusService _statusService;

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="statusService"></param>
        public StatusController(IStatusService statusService)
        {
            _statusService = statusService ?? throw new ArgumentNullException(nameof(statusService));
        }

        /// <summary>
        ///     Получение списка статусов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(policy: "ApiScope", Roles = "Admin, Courier, Manager, Seller")]
        public async Task<ActionResult<List<StatusResponse>>> GetStatusesAsync()
        {
            return await _statusService.GetStatusesAsync();
        }
    }
}