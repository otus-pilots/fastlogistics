﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FastLogistic.Models.Address;
using FastLogistic.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FastLogistic.Controllers
{
    /// <summary>
    ///     Адреса доставки
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class AddressController : ControllerBase
    {
        private readonly IAddressService  _addressService;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="addressService"></param>
        public AddressController(IAddressService addressService)
        {
            _addressService = addressService ?? throw new ArgumentNullException(nameof(addressService));
        }

        /// <summary>
        /// Получить список всех адресов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(policy: "ApiScope")]
        public async Task<List<AddressResponse>> GetAddressesAsync()
        {
            return await _addressService.GetAddressesAsync();
        }

        /// <summary>
        /// Получить данные об адресе по ID
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        [Authorize(policy: "ApiScope", Roles = "Admin, Courier, Registered, Manager, Seller")]
        public async Task<ActionResult<AddressResponse>> GetAddressByIdAsync(int id)
        {
            var address = await _addressService.GetAddressByIdAsync(id);

            if (address == null)
                return NotFound();

            return address;
        }

        /// <summary>
        /// Добавить новый адрес
        /// </summary>
        [HttpPost]
        [Authorize(policy: "ApiScope", Roles = "Admin, Courier, Registered, Manager, Seller")]
        public async Task<ActionResult<int>> AddAdressAsync(AddAddressRequest request)
        {
            var id = await _addressService.AddAdressAsync(request);

            return Ok(id);
        }

        /// <summary>
        /// Обновить данные об адресе
        /// </summary>
        [HttpPut]
        [Authorize(policy: "ApiScope", Roles = "Admin, Courier, Registered, Manager, Seller")]
        public async Task<IActionResult> UpdateAddressAsync(UpdateAddressRequest request)
        {
            var updated = await _addressService.UpdateAddressAsync(request);

            if (updated == null)
                return NotFound(request.Id);

            return Ok();
        }

        /// <summary>
        /// Удалить адрес
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id:int}")]
        [Authorize(policy: "ApiScope", Roles = "Admin, Courier, Registered, Manager, Seller")]
        public async Task<ActionResult> DeleteAddressAsync(int id)
        {

            var deleted = await _addressService.DeleteAddressAsync(id);
            if (deleted == null)
                return NotFound(id);

            return NoContent();
        }
    }
}
