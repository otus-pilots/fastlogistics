﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FastLogistic.Models.Zone;
using FastLogistic.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FastLogistic.Controllers
{
    /// <summary>
    ///     Адреса доставки
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class ZoneController : ControllerBase
    {
        private IZoneService _zoneService;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="zoneService"></param>
        public ZoneController(IZoneService zoneService)
        {
            _zoneService = zoneService ?? throw new ArgumentNullException(nameof(zoneService));
        }

        /// <summary>
        ///     Получить список всех зон доставки
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(policy: "ApiScope", Roles = "Admin, Courier, Registered, Manager, Seller")]
        public async Task<ActionResult<List<ZoneResponse>>> GetZoneAsync()
        {
            return await _zoneService.GetZoneAsync();
        }

        /// <summary>
        ///     Получить данные о зоне доставки по ID
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        [Authorize(policy: "ApiScope", Roles = "Admin, Courier, Registered, Manager, Seller")]
        public async Task<ActionResult<ZoneResponse>> GetZoneByIdAsync(int id)
        {
            var zone = await _zoneService.GetZoneByIdAsync(id);

            if (zone == null)
                return NotFound();

            return zone;
        }

        /// <summary>
        /// Добавление новой зоны доставки
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(policy: "ApiScope", Roles = "Admin, Courier, Registered, Manager, Seller")]
        public async Task<ActionResult<int>> CreateZoneAsync(AddZoneRequest request)
        {
            var id = await _zoneService.CreateZoneAsync(request);

            return Ok(id);
        }

        /// <summary>
        /// Обновление информации о зоне доставки
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [Authorize(policy: "ApiScope", Roles = "Admin, Courier, Registered, Manager, Seller")]
        public async Task<ActionResult> UpdateZoneAsync(UpdateZoneRequest request)
        {
            var zone = await _zoneService.UpdateZoneAsync(request);

            if (zone is null)
                return NotFound();

            return Ok();
        }

        /// <summary>
        ///     Удаление зоны доставки
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id:int}")]
        [Authorize(policy: "ApiScope", Roles = "Admin, Courier, Registered, Manager, Seller")]
        public async Task<ActionResult> DeleteZoneAsync(int id)
        {
            var zone = await _zoneService.DeleteZoneAsync(id);

            if (zone is null)
                return NotFound();

            return Ok();
        }
    }
}