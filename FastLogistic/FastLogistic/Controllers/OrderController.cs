﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using FastLogistic.Models.Order;
using FastLogistic.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;

namespace FastLogistic.Controllers
{
    /// <summary>
    ///     Заказы на доставку
    /// </summary>
    [Route("[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;

        /// <summary>
        ///     Конструктор.
        /// </summary>
        /// <param name="orderService"></param>
        public OrderController(
            IOrderService orderService)
        {
            _orderService = orderService ?? throw new ArgumentNullException(nameof(orderService));
        }

        /// <summary>
        ///     Получение списка заказов.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(policy: "ApiScope", Roles = "Admin, Courier, Manager, Seller")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(List<OrderShortResponse>))]
        public async Task<ActionResult<List<OrderShortResponse>>> GetOrdersAsync()
        {
            return Ok(await _orderService.GetOrdersAsync());
        }

        /// <summary>
        ///     Получение фильтрованного списка заказов
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("/[controller]s")]
        [Authorize(policy: "ApiScope", Roles = "Admin, Courier, Manager, Seller")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(List<OrderShortResponse>))]
        public async Task<ActionResult<List<OrderShortResponse>>> GetOrdersFilteredAsync(
            GetOrdersFilterRequest request = null)
        {
            var orders = await _orderService.GetOrdersFilteredAsync(request);

            return Ok(orders);
        }

        /// <summary>
        ///     Возвращает информацию о заказе.
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        [Authorize(policy: "ApiScope", Roles = "Admin, Courier, Manager, Seller")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(OrderShortResponse))]
        public async Task<ActionResult<OrderResponse>> GetOrderByIdAsync(int id)
        {
            var order = await _orderService.GetOrderByIdAsync(id);

            if (order == null)
                return NotFound();
            
            return Ok(order);
        }

        /// <summary>
        ///     Возвращает информацию о заказе по трек номеру.
        /// </summary>
        /// <returns></returns>
        [HttpGet("trackNumber/{trackNumber:long}")]
        [AllowAnonymous]
        [SwaggerResponse(HttpStatusCode.OK, typeof(OrderShortResponse))]
        public async Task<ActionResult<OrderResponse>> GetOrderByTrackAsync(Int64 trackNumber)
        {
            var order = await _orderService.GetOrderByTrackAsync(trackNumber);

            if (order == null)
                return NotFound();

            return Ok(order);
        }


        /// <summary>
        ///     Добавление нового заказа.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.Created, typeof(int))]
        [Authorize(policy: "ApiScope", Roles = "Admin, Manager, Seller")]
        public async Task<ActionResult<int>> CreateOrderAsync(AddOrderRequest request)
        {

            var id = await _orderService.CreateOrderAsync(request);
            if (id is null)
                return NotFound();

            return Ok(id.Value);
        }

        /// <summary>
        ///     Обновление информации о заказе.
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [SwaggerResponse(HttpStatusCode.NoContent, typeof(int))]
        [Authorize(policy: "ApiScope", Roles = "Admin, Courier, Seller")]
        public async Task<IActionResult> UpdateOrderAsync(UpdateOrderRequest request)
        {
            var updated = await _orderService.UpdateOrderAsync(request);

            if (updated is null)
                return NotFound();

            return Ok();
        }

        /// <summary>
        ///     Удаление заказа
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id:int}")]
        [Authorize(policy: "ApiScope", Roles = "Admin, Manager, Seller")]
        public async Task<IActionResult> DeleteOrderAsync(int id)
        {
            var deleted = await _orderService.DeleteOrderAsync(id);

            if (deleted is null)
                return NotFound();

            return Ok();
        }

        /// <summary>
        ///     Назначение курьера на заказ
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Route("/[controller]/AssignCourier")]
        [HttpPost("{orderId:int}/{userId:int}")]
        [Authorize(policy: "ApiScope", Roles = "Admin, Courier")]
        public async Task<IActionResult> AssignCourierToOrder(int orderId, int userId)
        {
            var assigned = await _orderService.AssignCourierToOrderAsync(orderId, userId);

            if (assigned is null)
                return NotFound();

            return Ok();
        }
    }
}