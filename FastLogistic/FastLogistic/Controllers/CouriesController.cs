﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FastLogistic.Models.Courier;
using FastLogistic.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FastLogistic.Controllers
{
    /// <summary>
    ///     Курьеры
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class CourierController : ControllerBase
    {
        private readonly ICourierService _courierService;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="courierService"></param>
        public CourierController(ICourierService courierService)
        {
            _courierService = courierService ?? throw new ArgumentNullException(nameof(courierService));
        }

        /// <summary>
        ///     Получить список всех курьеров
        /// </summary>
        [HttpGet]
        [Authorize(policy: "ApiScope", Roles = "Admin")]
        public async Task<ActionResult<List<CourierShortResponse>>> GetCouriersAsync()
        {
            return Ok(await _courierService.GetCouriersAsync());
        }

        /// <summary>
        ///     Получить данные курьера по id
        /// </summary>
        [HttpGet("{id:int}")]
        [Authorize(policy: "ApiScope", Roles = "Admin, Courier")]
        public async Task<ActionResult<CourierResponse>> GetCourierByIdAsync(int id)
        {
            var courier = await _courierService.GetCourierByIdAsync(id);

            if (courier == null)
                return NotFound();

            return Ok(courier);
        }

        /// <summary>
        ///     Добавление нового курьера
        /// </summary>
        [HttpPost]
        [Authorize(policy: "ApiScope", Roles = "Admin")]
        public async Task<IActionResult> AddCourierAsync(AddCourierRequest request)
        {
            var id = await _courierService.AddCourierAsync(request);

            return Ok(id);
        }

        /// <summary>
        ///     Обновить данные о курьере
        /// </summary>
        [HttpPut]
        [Authorize(policy: "ApiScope", Roles = "Admin, Courier")]
        public async Task<IActionResult> UpdateCourierAsync(UpdateCourierRequest request)
        {
            var updated = await _courierService.UpdateCourierAsync(request);

            if (updated is null)
                return NotFound();

            return Ok();
        }

        /// <summary>
        ///     Удаление курьера
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id:int}")]
        [Authorize(policy: "ApiScope", Roles = "Admin")]
        public async Task<IActionResult> DeleteCourierAsync(int id)
        {
            var courier = await _courierService.DeleteCourierAsync(id);

            if (courier is null)
                return NotFound();

            return Ok();
        }
    }
}