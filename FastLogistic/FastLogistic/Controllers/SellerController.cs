using FastLogistic.Models.Seller;
using FastLogistic.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FastLogistic.Controllers
{
    /// <summary>
    ///     Продавцы
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class SellerController : ControllerBase
    {
        private readonly ISellerService _sellerService;

        /// <summary>
        /// Кончтруктор.
        /// </summary>
        /// <param name="sellerService"></param>
        public SellerController(ISellerService sellerService)
        {
            _sellerService = sellerService ?? throw new ArgumentNullException(nameof(sellerService));
        }

        /// <summary>
        ///     Получение списка продавцов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(policy: "ApiScope", Roles = "Admin")]
        public async Task<ActionResult<List<SellerShortResponse>>> GetSellersAsync()
        {
            return await _sellerService.GetSellersAsync();
        }

        /// <summary>
        ///     Получение подробной информации о продавце по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        [Authorize(policy: "ApiScope", Roles = "Admin, Seller")]
        public async Task<ActionResult<SellerResponse>> GetSellerAsync(int id)
        {
            var seller = await _sellerService.GetSellerAsync(id);

            if (seller is null)
                return NotFound();

            return Ok(seller);
        }

        /// <summary>
        ///     Добавление (регистрация) нового продавца
        /// </summary>
        /// <param name="sellerRequest"></param>
        /// <returns>ID добавленного продавца</returns>
        [HttpPost]
        [Authorize(policy: "ApiScope", Roles = "Admin")]
        public async Task<ActionResult<int>> AddSellerAsync(AddSellerRequest sellerRequest)
        {
            var id = await _sellerService.AddSellerAsync(sellerRequest);

            return Ok(id);
        }

        /// <summary>
        ///     Обновление данных о продавце
        /// </summary>
        /// <param name="sellerRequest"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize(policy: "ApiScope", Roles = "Admin, Seller")]
        public async Task<IActionResult> UpdateSellerAsync(UpdateSellerRequest sellerRequest)
        {
            var updated = await _sellerService.UpdateSellerAsync(sellerRequest);

            if (updated is null)
                return NotFound();

            return Ok();
        }

        /// <summary>
        ///     Удаление продавца
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [Authorize(policy: "ApiScope", Roles = "Admin")]
        public async Task<ActionResult> DeleteSellerAsync(int id)
        {
            var deleted = await _sellerService.DeleteSellerAsync(id);

            if (deleted is null)
                return NotFound();

            return Ok();
        }
    }
}