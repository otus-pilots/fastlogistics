using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using FastLogistic.Models.Order;
using FastLogistic.Models.User;
using FastLogistic.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;

namespace FastLogistic.Controllers
{
    /// <summary>
    ///     Пользователи
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="userService"></param>
        public UserController(IUserService userService)
        {
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
        }

        /// <summary>
        ///     Получение списка пользователей
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(policy: "ApiScope", Roles = "Admin")]
        public async Task<ActionResult<List<UserShortResponse>>> GetUsersAsync()
        {
            return await _userService.GetUsersAsync();
        }

        /// <summary>
        ///     Получение подробной информации о пользователе по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        [Authorize(policy: "ApiScope", Roles = "Admin, Courier, Registered, Manager, Seller")]
        public async Task<ActionResult<UserResponse>> GetUserAsync(int id)
        {
            var user = await _userService.GetUserAsync(id);

            if (user is null)
                return NotFound();

            return Ok(user);
        }

        /// <summary>
        ///     Получение заказов прикреплённых к пользователю.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}/orders")]
        [Authorize(policy: "ApiScope", Roles = "Admin, Courier, Registered, Manager, Seller")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(List<OrderShortResponse>))]
        public async Task<ActionResult<OrderShortResponse>> GetUserOrdersAsync(int id)
        {
            var orders = await _userService.GetUserOrdersByIdAsync(id);

            if (orders is null)
                return NotFound();

            return Ok(orders);
        }

        /// <summary>
        ///     Добавление (регистрация) нового пользователя
        /// </summary>
        /// <param name="userRequest"></param>
        /// <returns>ID добавленного пользователя</returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult<int>> AddUserAsync(AddUserRequest userRequest)
        {
            var id = await _userService.AddUserAsync(userRequest);

            return Ok(id);
        }

        /// <summary>
        ///     Изменение информации о пользователе.
        /// </summary>
        /// <param name="userRequest"></param>
        /// <returns></returns>
        [HttpPut("info/update")]
        [Authorize(policy: "ApiScope", Roles = "Admin, Courier, Registered, Manager, Seller")]
        public async Task<ActionResult> UpdateUserInfoAsync(UpdateUserInfoRequest userRequest)
        {
            var updated = await _userService.UpdateUserInfoAsync(userRequest);

            if (updated is null)
                return NotFound();

            return Ok();
        }

        /// <summary>
        ///     Изменение роли пользователя.
        /// </summary>
        /// <param name="userRequest"></param>
        /// <returns></returns>
        [HttpPut("role/update")]
        [Authorize(policy: "ApiScope", Roles = "Admin")]
        public async Task<ActionResult> UpdateUserRoleAsync(UpdateUserRoleRequest userRequest)
        {
            var updated = await _userService.UpdateUserRoleAsync(userRequest);

            if (updated is null)
                return NotFound();

            return Ok();
        }


        /// <summary>
        ///     Удаление пользователя
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [Authorize(policy: "ApiScope", Roles = "Admin")]
        public async Task<ActionResult> DeleteUserAsync(int id)
        {
            var user = await _userService.DeleteUserAsync(id);

            if (user is null)
                return NotFound();

            return Ok();
        }
    }
}