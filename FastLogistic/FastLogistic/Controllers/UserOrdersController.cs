﻿using FastLogistic.Models.UserOrder;
using FastLogistic.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace FastLogistic.Controllers
{
    /// <summary>
    ///     Статусы заказа
    /// </summary>
    [Route("[controller]")]
    [ApiController]
    public class UserOrdersController : ControllerBase
    {

        private readonly IUserOrderService _userOrderService;

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="orderStatusService"></param>
        public UserOrdersController(
            IUserOrderService orderStatusService)
        {
            _userOrderService = orderStatusService ?? throw new ArgumentNullException(nameof(orderStatusService));
        }


        /// <summary>
        ///     Добавление связи заказа с пользователем по трек номеру.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(policy: "ApiScope", Roles = "Admin, Registered")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(IActionResult))]
        public async Task<IActionResult> AddUserOrderAsync(AddUserOrderRequest request)
        {
            var order = await _userOrderService.AddUserOrderAsync(request);
            if (order is false)
                return NotFound();

            return Ok();
        }


    }


}
