using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using FastLogistic.Models.OrderStatus;
using FastLogistic.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;

namespace FastLogistic.Controllers
{
    /// <summary>
    ///     Статусы заказа
    /// </summary>
    [Route("[controller]")]
    [ApiController]
    public class OrderStatusController : ControllerBase
    {
        private readonly IOrderStatusService _orderStatusService;

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="orderStatusService"></param>
        public OrderStatusController(
            IOrderStatusService orderStatusService)
        {
            _orderStatusService = orderStatusService ?? throw new ArgumentNullException(nameof(orderStatusService));
        }

        /// <summary>
        ///     Получение истории статусов заказа по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        [Authorize(policy: "ApiScope", Roles = "Admin, Courier, Manager, Seller")]
        [SwaggerResponse(HttpStatusCode.OK,
            typeof(OrderStatusResponse))]
        public async Task<ActionResult<List<OrderStatusResponse>>> GetOrderStatusesByIdAsync(int id)
        {
            var orderStatuses = await _orderStatusService.GetOrderStatusesByIdAsync(id);
            if (orderStatuses is null)
                return NotFound();

            return orderStatuses;
        }

        /// <summary>
        ///     Изменение статуса заказа (добавление новой записи OrderStatus)
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK,
            typeof(IActionResult))]
        [Authorize(policy: "ApiScope", Roles = "Admin, Courier, Manager, Seller")]
        public async Task<IActionResult> AddOrderStatusAsync(AddOrderStatusRequest request)
        {
            var order = await _orderStatusService.AddOrderStatusAsync(request);
            if (order is null)
                return NotFound();

            return Ok();
        }
    }
}