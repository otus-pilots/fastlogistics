﻿using FastLogistic.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Notifications
{
    public class NotificationManagerFake : INotificationManager
    {
        public void Send(Order order)
        {
        }
    }
}
