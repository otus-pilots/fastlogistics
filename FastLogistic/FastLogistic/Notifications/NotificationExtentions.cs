﻿using EventBusRabbitMQ;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Notifications
{
    public static class NotificationExtentions
    {
        /// <summary>
        /// Добаляет сервисы бизнес логики.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IServiceCollection AddNotification(this IServiceCollection services, IConfiguration configuration)
        {
            if (configuration.GetValue<bool>("Notification"))
            {
                services.AddEventBus(configuration);
                services
                    .AddSingleton<NotificationContext>()
                    .AddSingleton<OrderStatusEmailNotification>()
                    .AddSingleton<OrderStatusSMSNotification>()
                    .AddScoped<INotificationManager, NotificationManager>()
                    ;
            }
            else
            {
                services.AddScoped<INotificationManager, NotificationManagerFake>();
            }
            return services;
        }

    }
}
