﻿using FastLogistic.Core.Domain;
using FastLogistic.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Notifications
{
    public interface INotification
    {
        /// <summary>
        /// Отправляет сообщения-оповещения в event bus.
        /// </summary>
        void SendNotification(List<User> users, Int64 trackNumber, String status);
    }
}
