﻿using BusinesEvents;
using EventBusInfrastructure.Abstraction;
using FastLogistic.Core.Domain;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Notifications
{
    public class OrderStatusSMSNotification : INotification
    {
        private readonly ILogger<OrderStatusSMSNotification> _logger;
        private readonly IEventBus _eventBus;

        public OrderStatusSMSNotification(
            ILogger<OrderStatusSMSNotification> logger,
            IEventBus eventBus)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
        }

        public void SendNotification(List<User> users, Int64 trackNumber, String status)
        {
            foreach (var user in users)
            {
                if (String.IsNullOrEmpty(user.PhoneNumber))
                {
                    continue;
                }

                using var notificationEvent = new OrderStatusSMSNotifEvent(
                    user.PhoneNumber,
                    trackNumber,
                        status);

                _logger.LogInformation("----- Publishing integration event: " +
                    "{IntegrationEventId} - ({@IntegrationEvent})",
                    notificationEvent.Id, notificationEvent);

                _eventBus.Publish(notificationEvent);
            }
        }
    }
}
