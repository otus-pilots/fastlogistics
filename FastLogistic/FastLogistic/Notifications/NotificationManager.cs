﻿using FastLogistic.Core.Abstractions.Repositories;
using FastLogistic.Core.Domain;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Notifications
{
    public class NotificationManager : INotificationManager
    {
        private readonly ILogger<NotificationManager> _logger;
        private readonly List<NotificationContext> _notificationContexts = new List<NotificationContext>();
        private readonly OrderStatusEmailNotification _emailNotification;
        private readonly OrderStatusSMSNotification _smsNotification;


        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="emailNotification"></param>
        /// <param name="smsNotification"></param>
        public NotificationManager(
            ILogger<NotificationManager> logger,
            OrderStatusEmailNotification emailNotification,
            OrderStatusSMSNotification smsNotification
            )
        {
            _emailNotification = emailNotification ?? throw new ArgumentNullException(nameof(emailNotification));
            _smsNotification = smsNotification ?? throw new ArgumentNullException(nameof(smsNotification));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

        }
        /// <summary>
        /// Отправка уведомления об изменении статуса заказа.
        /// </summary>
        /// <param name="order"></param>
        public void Send(Order order)
        {
            //Список статусов на оповещение. Нужно подумать как лучше сделать
            // var notificationStatuses = new List<string>() { "InWork", "Accepted", "Delivered", "Cancelled", "Returning" };
            var statuses = order.OrderStatuses;
            var status = order.Statuses.Where(x => x.Id == statuses.OrderBy(y => y.TimeChanged).Last().StatusId).First().Name;

            //Если статус не в списке разрешённых, то выходим
            // if (!notificationStatuses.Contains(status))
            // {
            //     return;
            // }

            _notificationContexts.Add(new NotificationContext(_emailNotification));

            _notificationContexts.Add(new NotificationContext(_smsNotification));

            foreach (var context in _notificationContexts)
            {
                context.Execute(order.Users, order.TrackNumber, status);
            }

        }
    }

}
