﻿using FastLogistic.Core.Domain;
using System.Threading.Tasks;

namespace FastLogistic.Notifications
{
    /// <summary>
    /// Менеджер отправки сообщений.
    /// </summary>
    public interface INotificationManager
    {
        /// <summary>
        /// отправка оповещений по заказу.
        /// </summary>
        /// <param name="order"></param>
        void Send(Order order);
    }
}
