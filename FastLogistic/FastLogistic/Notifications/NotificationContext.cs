﻿using FastLogistic.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Notifications
{
    public class NotificationContext
    {
        private INotification _notification;

        public NotificationContext()
        {

        }
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="notification"></param>
        public NotificationContext(INotification notification)
        {
            _notification = notification;
        }


        /// <summary>
        /// Устанавливает объект оповещения.
        /// </summary>
        /// <param name="notification">Объект оповещения.</param>
        public void SetNotification(INotification notification)
        {
            _notification = notification;
        }

        /// <summary>
        /// Запуск выполнения отправки оповещений.
        /// </summary>
        /// <param name="users"></param>
        /// <param name="trackNumber"></param>
        /// <param name="status"></param>
        public void Execute(List<User> users, Int64 trackNumber, String status)
        {
            _notification.SendNotification(users, trackNumber,  status);
        }

    }
}
