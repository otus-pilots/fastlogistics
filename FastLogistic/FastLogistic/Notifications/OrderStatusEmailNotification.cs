﻿using BusinesEvents;
using EventBusInfrastructure.Abstraction;
using FastLogistic.Core.Domain;
using FastLogistic.Models.User;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Notifications
{
    /// <summary>
    /// Оповещение пользователя по email.
    /// </summary>
    public class OrderStatusEmailNotification : INotification
    {
        private readonly ILogger<OrderStatusEmailNotification> _logger;
        private readonly IEventBus _eventBus;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="eventBus"></param>
        public OrderStatusEmailNotification(
            ILogger<OrderStatusEmailNotification> logger,
            IEventBus eventBus)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
        }

        /// <summary>
        /// Отправка сообщения в Rabbitmq
        /// </summary>
        /// <param name="users"></param>
        /// <param name="trackNumber"></param>
        /// <param name="status"></param>
        public void SendNotification(List<User> users, Int64 trackNumber, String status)
        {
            foreach (var user in users)
            {

                if (user.Email == null)
                {
                    continue;
                }

                using var notificationEvent = new OrderStatusEmailNotifEvent(
                    user.Email,
                    trackNumber,
                    status);

                _logger.LogInformation("----- Publishing integration event: " +
                    "{IntegrationEventId}  - ({@IntegrationEvent})",
                    notificationEvent.Id, notificationEvent);

                _eventBus.Publish(notificationEvent);
            }
        }

    }
}
