﻿using FastLogistic.Models.Statuses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Hubs
{
    public interface IStatusOrderClientHub
    {
        /// <summary>
        /// Оповещает об изменении статуса заказа.
        /// </summary>
        /// <param name="orderId">Id заказа.</param>
        /// <param name="newStatusId">Новый статус.</param>
        public Task ModuleStatusChanged(int orderId, int newStatusId);
    }
}
