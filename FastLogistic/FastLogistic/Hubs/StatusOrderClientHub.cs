﻿using FastLogistic.Models.Statuses;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Hubs
{
    /// <summary>
    /// Хаб менеджера статусов.
    /// </summary>
    public class StatusOrderClientHub : Hub
    {
        /// <summary>
        /// Подписка на группу.
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public async Task SubscribeOnOrder(int orderId)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, "Order" + orderId.ToString());
        }

        /// <summary>
        /// Выход из группы оповещений.
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public async Task UnSubscribeOnOrder(int orderId)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, "Order" + orderId.ToString());
        }

        /// <summary>
        /// Оповещение членов группы(кроме себя) об изменении статуса.
        /// </summary>
        /// <param name="orderId">Id заказа</param>
        /// <param name="newStatusId">Новый статус</param>
        /// <returns></returns>
        public async Task NotifyOthersStatusChenged(int orderId, int newStatusId)
        {
            await Clients.OthersInGroup("Order" + orderId.ToString()).SendAsync("NotifyOthersStatusChenged", orderId, newStatusId);
        }
    }
}
