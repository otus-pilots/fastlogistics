﻿using System.ComponentModel.DataAnnotations;

namespace FastLogistic.Models.Seller
{
    public class AddSellerRequest
    {
        public string Name { get; set; }

        public int UserId { get; set; }
    }
}
