﻿namespace FastLogistic.Models.Seller
{
    public class SellerShortResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
