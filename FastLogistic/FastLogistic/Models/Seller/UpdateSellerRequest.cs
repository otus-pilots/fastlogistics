﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FastLogistic.Models.Seller
{
    public class UpdateSellerRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
