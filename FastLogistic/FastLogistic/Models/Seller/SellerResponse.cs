﻿using FastLogistic.Models.User;

namespace FastLogistic.Models.Seller
{
    public class SellerResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public UserShortResponse User { get; set; }
    }
}
