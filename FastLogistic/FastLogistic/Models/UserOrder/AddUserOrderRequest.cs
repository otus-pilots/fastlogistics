﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Models.UserOrder
{
    /// <summary>
    ///     Информация о привязке заказа.
    /// </summary>
    public class AddUserOrderRequest
    {
        /// <summary>
        ///     Id пользователя.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        ///     Id заказа.
        /// </summary>
        public int OrderId { get; set; }
    }

}
