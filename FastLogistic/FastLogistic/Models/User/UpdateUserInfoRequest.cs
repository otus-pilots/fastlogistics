namespace FastLogistic.Models.User
{
    public class UpdateUserInfoRequest : AddUserRequest
    {
        public int Id { get; set; }
    } 
}