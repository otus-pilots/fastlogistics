﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Models.User
{
    public class UpdateUserRoleRequest
    {
        public int Id { get; set; }
        public int RoleId { get; set; }

    }
}
