namespace FastLogistic.Models.User
{
    public class UserResponse
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public int RoleId { get; set; }
        public int? SellerId { get; set; }
        public int? CourierId { get; set; }
        public string PhoneNumber { get; set; }

    }
}