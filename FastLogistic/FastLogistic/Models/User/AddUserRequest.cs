using System.ComponentModel.DataAnnotations;

namespace FastLogistic.Models.User
{
    public class AddUserRequest
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}