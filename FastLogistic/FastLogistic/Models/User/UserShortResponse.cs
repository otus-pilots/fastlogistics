namespace FastLogistic.Models.User
{
    public class UserShortResponse
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
    }
}