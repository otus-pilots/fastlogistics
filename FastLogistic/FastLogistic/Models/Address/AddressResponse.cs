﻿namespace FastLogistic.Models.Address
{
    public class AddressResponse
    {
        /// <summary>
        ///     Id адреса
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     Адрес заказа
        /// </summary>
        public string RegistrationAddress { get; set; }

        /// <summary>
        ///     ID зоны доставки, к которой относится данный адрес
        /// </summary>
        public int ZoneId { get; set; }
    }
}