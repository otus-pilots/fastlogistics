﻿using System.ComponentModel.DataAnnotations;

namespace FastLogistic.Models.Address
{
    /// <summary>
    /// Модель добавдения адреса.
    /// </summary>
    public class AddAddressRequest
    {
        /// <summary>
        /// Адрес.
        /// </summary>
        public string RegistrationAddress { get; set; }

        /// <summary>
        /// Зона принадлежности адреса.
        /// </summary>
        public int ZoneId { get; set; }
    }
}
