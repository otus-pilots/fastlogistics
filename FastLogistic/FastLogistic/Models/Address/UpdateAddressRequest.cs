﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FastLogistic.Models.Address
{
    public class UpdateAddressRequest
    {
        public int Id { get; set; }

        public string RegistrationAddress { get; set; }

        public int ZoneId { get; set; }
    }
}
