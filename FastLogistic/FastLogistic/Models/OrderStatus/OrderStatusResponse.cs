using System;

namespace FastLogistic.Models.OrderStatus
{
    /// <summary>
    ///     Информация о статусе заказа
    /// </summary>
    public class OrderStatusResponse
    {
        /// <summary>
        ///     Дата/время изменения статуса заказа
        /// </summary>
        public DateTime TimeChanged { get; set; }

        /// <summary>
        ///     Статус заказа
        /// </summary>
        public string StatusName { get; set; }
    }
}