namespace FastLogistic.Models.OrderStatus
{
    /// <summary>
    ///     Информация о статусе заказа
    /// </summary>
    public class AddOrderStatusRequest
    {
        /// <summary>
        ///     Id заказа
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        ///     Id статуса
        /// </summary>
        public int StatusId { get; set; }
    }
}