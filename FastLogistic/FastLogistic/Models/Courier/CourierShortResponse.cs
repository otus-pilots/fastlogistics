﻿namespace FastLogistic.Models.Courier
{
    public class CourierShortResponse
    {
        public int Id { get; set; }

        public double BaseRate { get; set; }

        public double BaseTime { get; set; }

        public double MaxWeight { get; set; }

        public double WeightRate { get; set; }
    }
}
