﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FastLogistic.Models.Courier
{
    public class AddCourierRequest
    {
        public int? UserId { get; set; }

        public double? BaseRate { get; set; }

        public double? BaseTime { get; set; }

        public double? MaxWeight { get; set; }

        public double? WeightRate { get; set; }

        public List<int> Zones { get; set; }
    }
}
