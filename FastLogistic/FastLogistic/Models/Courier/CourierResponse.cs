﻿using FastLogistic.Models.User;

namespace FastLogistic.Models.Courier
{
    public class CourierResponse
    {
        public int Id { get; set; }

        public double BaseRate { get; set; }

        public double BaseTime { get; set; }

        public double MaxWeight { get; set; }

        public double WeightRate { get; set; }

        public UserShortResponse User { get; set; }
    }
}
