﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Models.Order
{
    /// <summary>
    /// Информация о заказе.
    /// </summary>
    public class OrderInfo
    {

        /// <summary>
        /// Статус заказа.
        /// </summary>
        public String Status { get; set; }

    }
}
