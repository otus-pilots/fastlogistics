﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Models.Order
{
    /// <summary>
    /// Модель обновления данных заказа.
    /// </summary>
    public class UpdateOrderRequest
    {
        public int Id { get; set; }
        /// <summary>
        ///     Вес заказа
        /// </summary>
        public decimal Weight { get; set; }

        /// <summary>
        ///     Предполагаемое время доставки
        /// </summary>
        public DateTime PreferredTime { get; set; }

        /// <summary>
        ///     Предполагаемая стоимость
        /// </summary>
        public decimal PreferredCost { get; set; }

        /// <summary>
        ///     Трек-номер
        /// </summary>
        public long TrackNumber { get; set; }

        /// <summary>
        ///     Id адреса отправления
        /// </summary>
        public int AddressFromId { get; set; }

        /// <summary>
        ///     Id адреса назначения
        /// </summary>
        public int AddressToId { get; set; }

    }
}
