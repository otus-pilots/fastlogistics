﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Models.Order
{
    /// <summary>
    /// Модель добавления заказа.
    /// </summary>
    public class AddOrderRequest
    {
        /// <summary>
        ///     Вес заказа
        /// </summary>
        public decimal Weight { get; set; }

        /// <summary>
        ///     Предполагаемое время доставки
        /// </summary>
        public DateTime PreferredTime { get; set; }

        /// <summary>
        ///     Предполагаемая стоимость
        /// </summary>
        public decimal PreferredCost { get; set; }

        /// <summary>
        ///     Трек-номер
        /// </summary>
        public long TrackNumber { get; set; }

        /// <summary>
        ///     Id адреса отправления
        /// </summary>
        public int AddressFromId { get; set; }

        /// <summary>
        ///     Id адреса назначения
        /// </summary>
        public int AddressToId { get; set; }
        
        /// <summary>
        ///     Id продавца, разместившего заказ на доставку
        /// </summary>
        public int SellerId { get; set; }
        
        /// <summary>
        ///     Id статуса создаваемого заказа
        /// </summary>
        public int StatusId { get; set; }
    }
}
