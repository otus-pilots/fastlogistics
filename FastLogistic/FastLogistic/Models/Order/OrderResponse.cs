﻿using System;

namespace FastLogistic.Models.Order
{
    public class OrderResponse
    {   
        public int Id { get; set; }
        /// <summary>
        ///     Вес заказа
        /// </summary>
        public decimal Weight { get; set; }

        /// <summary>
        ///     Предполагаемое время доставки
        /// </summary>
        public DateTime PreferredTime { get; set; }

        /// <summary>
        ///     Предполагаемая стоимость
        /// </summary>
        public decimal PreferredCost { get; set; }

        /// <summary>
        ///     Трек-номер
        /// </summary>
        public long TrackNumber { get; set; }

        /// <summary>
        ///  Навигационное свойство для связи с курьером
        /// </summary>
        public Core.Domain.Courier Courier { get; set; }

        /// <summary>
        ///     Навигационное свойство для связи с продавцом
        /// </summary>
        public Core.Domain.Seller Seller { get; set; }

        /// <summary>
        ///     Адрес отправления
        /// </summary>
        public Core.Domain.Address AddressFrom { get; set; }

        /// <summary>
        ///     Адрес назначения
        /// </summary>
        public Core.Domain.Address AddressTo { get; set; }

        public String Status { get; set; }

    }
}
