﻿namespace FastLogistic.Models.Order
{
    /// <summary>
    ///     Краткое представление заказа на доставку
    /// </summary>
    public class OrderShortResponse
    {
        public int Id { get; set; }


        /// <summary>
        ///     Вес заказа.
        /// </summary>
        public decimal Weight { get; set; }

        /// <summary>
        ///     Трек-номер
        /// </summary>
        public long TrackNumber { get; set; }

        /// <summary>
        ///     Id курьера.
        /// </summary>
        public int? CourierId { get; set; }

        /// <summary>
        ///     Id адреса отправления
        /// </summary>
        public int AddressIdFromId { get; set; }

        /// <summary>
        ///     Id адреса назначения.
        /// </summary>
        public int AddressIdTo { get; set; }
    }
}