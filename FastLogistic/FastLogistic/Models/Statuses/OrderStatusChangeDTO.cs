﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Models.Statuses
{
    /// <summary>
    /// Информация о новом статусе заказа.
    /// </summary>
    public class OrderStatusChangeDTO
    {
        /// <summary>
        /// Идентификатор заказа.
        /// </summary>
        public int OrderId;

        /// <summary>
        /// Новый статус заказа.
        /// </summary>
        public String Status;

        public OrderStatusChangeDTO(int orderId, string status)
        {
            OrderId = orderId;
            Status = status;
        }
    }
}
