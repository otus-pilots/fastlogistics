namespace FastLogistic.Models.Statuses
{
    /// <summary>
    ///     Статус заказа
    /// </summary>
    public class StatusResponse
    {
        /// <summary>
        ///     Id статуса
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     Название статуса
        /// </summary>
        public string Name { get; set; }
    }
}