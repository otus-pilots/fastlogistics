using System.ComponentModel.DataAnnotations;

namespace FastLogistic.Models.Zone
{
    public class UpdateZoneRequest
    {
        public int Id { get; set; }

        public string Name { get; set; }
        
        public double CostRate { get; set; }

        public double TimeRate { get; set; }
    }
}