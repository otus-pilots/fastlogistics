namespace FastLogistic.Models.Zone
{
    public class AddZoneRequest
    {
        public string Name { get; set; }
        public double CostRate { get; set; }
        public double TimeRate { get; set; }
    }
}