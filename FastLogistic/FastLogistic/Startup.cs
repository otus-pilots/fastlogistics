using FastLogistic.DataAccess;
using FastLogistic.Hubs;
using FastLogistic.Middlewares;
using FastLogistic.Notifications;
using FastLogistic.Services;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NSwag.Generation.Processors.Security;
using Serilog;
using System;
using System.Collections.Generic;
using NSwag;
using NSwag.AspNetCore;

namespace FastLogistic
{
    internal class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // получаем строку подключения из файла конфигурации
            var connection = Configuration.GetConnectionString("DefaultConnection");
            services.AddDb(connection);
            
            services.AddStackExchangeRedisCache(options =>
                options.Configuration = Configuration.GetValue<string>("RedisConnection"));
            
            services.AddServicesBL();
            services.AddNotification(Configuration);

            services.AddCors();

            services.AddSignalR(options =>
            {
                options.EnableDetailedErrors = true;
                options.KeepAliveInterval = TimeSpan.FromSeconds(2);
            });

            services.AddControllers()
                // Добавляем валидаторы
                .AddFluentValidation(s =>
                {
                    s.RegisterValidatorsFromAssemblyContaining<Startup>();
                })
                .AddNewtonsoftJson(x => 
                    x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                    );

            services.AddOpenApiDocument(options =>
            {
                options.DocumentName = "v1";
                options.Title = "FastLogistic API";
                options.Version = "v1";

                var authorizationConfig = Configuration.GetSection("Authorization");

                options.AddSecurity("oauth2", new OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.OAuth2,
                    Flows = new OpenApiOAuthFlows
                    {
                        AuthorizationCode = new OpenApiOAuthFlow
                        {
                            AuthorizationUrl = authorizationConfig.GetValue<string>("AuthorizationUrl"),
                            TokenUrl =  authorizationConfig.GetValue<string>("TokenUrl"),
                            Scopes = new Dictionary<string, string> { { "api1", "FastLogistic API" } }
                        }
                    }
                }) ;
                
                options.OperationProcessors.Add(new OperationSecurityScopeProcessor("oauth2"));
            });
            services.AddAuthentication("Bearer")
                .AddJwtBearer("Bearer", options =>
                {
                    options.Audience = "api1";
                    options.Authority = Configuration.GetValue<string>("AuthorityURL");
                    options.RequireHttpsMetadata = false;
                });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("ApiScope", policy =>
                {
                    policy.RequireAuthenticatedUser()
                        .RequireClaim("scope", "api1");
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseHttpStatusCodeExceptionMiddleware();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder =>
            {
                builder.WithOrigins("http://localhost:3000");
                builder.AllowAnyHeader();
                builder.AllowAnyMethod();
                builder.AllowCredentials();
            });

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";

                x.OAuth2Client = new OAuth2ClientSettings
                {
                    ClientId = "swagger",
                    ClientSecret = null,
                    AppName = "Swagger UI",
                    UsePkceWithAuthorizationCodeGrant = true
                };
            });

            app.UseHttpsRedirection();

            app.UseSerilogRequestLogging();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<StatusOrderClientHub>("/fl/status", options =>
                {
                    Log.Debug("Сконфигурирован хаб /fl/status.");
                    /*options.Transports = HttpTransportType.WebSockets;*/
                });
            });
        }
    }
}