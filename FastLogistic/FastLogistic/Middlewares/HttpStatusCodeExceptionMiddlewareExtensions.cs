﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Middlewares
{
    /// <summary>
    /// Метод расширения для добавления middleware в цепочку вызовов HTTP .
    /// </summary>
    public static class HttpStatusCodeExceptionMiddlewareExtensions
    {
        /// <summary>
        /// Добавление обработчика ошибок в цепочку вызовов HTTP .
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseHttpStatusCodeExceptionMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<HttpStatusCodeExceptionMiddleware>();
        }
    }
}
