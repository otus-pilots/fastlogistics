﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace FastLogistic.Middlewares
{
    /// <summary>
    /// Обработчик неотловленных ошибок
    /// </summary>
    public class HttpStatusCodeExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="next">Request Delegate</param>
        public HttpStatusCodeExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, ILogger<HttpStatusCodeExceptionMiddleware> logger,
          IWebHostEnvironment hostingEnvironment)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                if (context.Response.HasStarted)
                {
                    logger.LogWarning(ex,
                      "Ответ уже запущен, промежуточное программное обеспечение http status code не будет выполнено.");

                    throw;
                }

                context.Response.Clear();
                context.Response.ContentType = "application/json";

                object error;
                int statusCode;

                if (ex is ValidationException validation)
                {
                    statusCode = StatusCodes.Status422UnprocessableEntity;
                    error = new
                    {
                        error = validation.ValidationResult.ErrorMessage
                    };
                }
                else
                {
                    statusCode = StatusCodes.Status500InternalServerError;
                    var isDevelopment = hostingEnvironment.IsDevelopment();

                    error = new
                    {
                        error = isDevelopment ? ex.Message : "Во время обработки вашего запроса произошла ошибка",
                        stackTrace = isDevelopment ? ex.StackTrace : null
                    };
                }

                context.Response.StatusCode = statusCode;
                var text = JsonSerializer.Serialize(error);

                await context.Response.WriteAsync(text);
            }
        }
    }
}
