﻿using FastLogistic.Models.Address;
using FluentValidation;

namespace FastLogistic.Validators
{
    /// <summary>
    /// Валидатор для <see cref="AddAddressRequest"/>.<br/>
    /// </summary>

    public class AddAddressValidator : AbstractValidator<AddAddressRequest>
    {
        /// <summary>
        /// Валидация модели добавления адреса 
        /// </summary>
        public AddAddressValidator()
        {
            RuleFor(x => x.RegistrationAddress).NotEmpty().NotNull().MaximumLength(254);
            RuleFor(x => x.ZoneId).NotEmpty().NotNull();
        }
    }

    /// <summary>
    /// Валидатор для <see cref="UpdateAddressRequest"/>.<br/>
    /// </summary>
    public class UpdateAddressValidator : AbstractValidator<UpdateAddressRequest>
    {
        /// <summary>
        /// Валидация модели обновления адреса 
        /// </summary>
        public UpdateAddressValidator()
        {
            RuleFor(x => x.RegistrationAddress).NotEmpty().NotNull().MaximumLength(254);
            RuleFor(x => x.ZoneId).NotEmpty().NotNull();
        }
    }

}
