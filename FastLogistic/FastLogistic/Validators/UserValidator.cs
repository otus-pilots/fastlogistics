﻿using FastLogistic.Models.Address;
using FastLogistic.Models.User;
using FluentValidation;
using System;

namespace FastLogistic.Validators
{
    /// <summary>
    /// Валидатор для <see cref="AddUserRequest"/>.<br/>
    /// </summary>

    public class AddUserValidator : AbstractValidator<AddUserRequest>
    {
        /// <summary>
        /// Валидация модели добавления пользователя. 
        /// </summary>
        public AddUserValidator()
        {
            RuleFor(x => x.Login).NotEmpty().WithMessage("Не указан логин").MaximumLength(64);
            RuleFor(x => x.Password).NotEmpty().WithMessage("Не указан пароль").MaximumLength(64);

            //поле PhoneNumber проверяется по формату если поле не пустое 
            RuleFor(x => x.PhoneNumber).Matches("^((8|\\+7)[\\-]?)?(\\(?\\d{3}\\)?[\\-]?)?[\\d\\-]{7,10}$").When(x => !String.IsNullOrEmpty(x.PhoneNumber)).WithMessage("{PropertyName} имеет неверный формат +7xxxxxxxxxx").MaximumLength(12);

            //Каскадная проверка. Остановка на первом несовпадении.
            RuleFor(x => x.Email).Cascade(CascadeMode.Stop).NotEmpty().WithMessage("{PropertyName} is required.")
                .EmailAddress()
                .WithMessage("A valid {PropertyName} is required.")
                .MaximumLength(254);
        }
    }

    public class UpdateUserInfoValidator : AbstractValidator<UpdateUserInfoRequest>
    {
        /// <summary>
        /// Валидация модели добавления пользователя. 
        /// </summary>
        public UpdateUserInfoValidator()
        {
            RuleFor(x => x.Login).NotEmpty().WithMessage("Не указан логин").MaximumLength(64);
            // RuleFor(x => x.Password).NotEmpty().WithMessage("Не указан пароль");
            RuleFor(x => x.Password).MaximumLength(64);

            //поле PhoneNumber проверяется по формату если поле не пустое 
            RuleFor(x => x.PhoneNumber).Matches("^((8|\\+7)[\\-]?)?(\\(?\\d{3}\\)?[\\-]?)?[\\d\\-]{7,10}$").When(x => x.PhoneNumber.Length > 0).WithMessage("{PropertyName} имеет неверный формат +7xxxxxxxxxx").MaximumLength(12);

            //Каскадная проверка. Остановка на первом несовпадении.
            RuleFor(x => x.Email).Cascade(CascadeMode.Stop).NotEmpty().WithMessage("{PropertyName} is required.")
                .EmailAddress()
                .WithMessage("A valid {PropertyName} is required.")
                .MaximumLength(254);
        }
    }

}
