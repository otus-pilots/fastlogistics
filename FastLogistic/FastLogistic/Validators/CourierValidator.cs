﻿using FastLogistic.Models.Courier;
using FluentValidation;

namespace FastLogistic.Validators
{
    /// <summary>
    /// Валидатор для <see cref="AddCourierRequest"/>.<br/>
    /// </summary>

    public class AddCourierValidator : AbstractValidator<AddCourierRequest>
    {
        /// <summary>
        /// Валидация модели добавления курьера.
        /// </summary>
        public AddCourierValidator()
        {
            RuleFor(x => x.UserId).NotEmpty().NotNull();
            RuleFor(x => x.BaseRate).NotEmpty().NotNull().LessThan(0.25).WithMessage("Минимальная ставка 0.25").GreaterThanOrEqualTo(5).WithMessage("Максимальная ставка 5");
            RuleFor(x => x.BaseTime).NotEmpty().NotNull().LessThan(0.25).WithMessage("Минимальное время 0.25").GreaterThanOrEqualTo(5).WithMessage("Максимальное время 5");
            RuleFor(x => x.MaxWeight).NotEmpty().NotNull().LessThan(1).WithMessage("Минимальный вес 1").GreaterThanOrEqualTo(25).WithMessage("Максимальный вес 25");
            RuleFor(x => x.WeightRate).NotEmpty().NotNull().LessThan(0.25).WithMessage("Ставка за вес минимальная 0.25").GreaterThanOrEqualTo(5).WithMessage("Ставка за вес максимальная 5");

        }
    }

    /// <summary>
    /// Валидатор для <see cref="UpdateCourierRequest"/>.<br/>
    /// </summary>

    public class UpdateCourierValidator : AbstractValidator<UpdateCourierRequest>
    {
        /// <summary>
        /// Валидация модели добавления курьера.
        /// </summary>
        public UpdateCourierValidator()
        {
            RuleFor(x => x.Id).NotEmpty().NotNull();
            RuleFor(x => x.BaseRate).NotEmpty().NotNull().LessThan(0.25).WithMessage("Минимальная ставка 0.25").GreaterThanOrEqualTo(5).WithMessage("Максимальная ставка 5");
            RuleFor(x => x.BaseTime).NotEmpty().NotNull().LessThan(0.25).WithMessage("Минимальное время 0.25").GreaterThanOrEqualTo(5).WithMessage("Максимальное время 5");
            RuleFor(x => x.MaxWeight).NotEmpty().NotNull().LessThan(1).WithMessage("Минимальный вес 1").GreaterThanOrEqualTo(25).WithMessage("Максимальный вес 25");
            RuleFor(x => x.WeightRate).NotEmpty().NotNull().LessThan(0.25).WithMessage("Ставка за вес минимальная 0.25").GreaterThanOrEqualTo(5).WithMessage("Ставка за вес максимальная 5");

        }
    }
}
