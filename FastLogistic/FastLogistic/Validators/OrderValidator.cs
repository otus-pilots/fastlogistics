﻿using FastLogistic.Models.Order;
using FluentValidation;

namespace FastLogistic.Validators
{
    /// <summary>
    /// Валидатор для <see cref="AddOrderRequest"/>.<br/>
    /// </summary>

    public class AddOrderValidator : AbstractValidator<AddOrderRequest>
    {
        /// <summary>
        /// Валидация модели добавления заказа
        /// </summary>
        public AddOrderValidator()
        {
            RuleFor(x => x.Weight).NotEmpty().NotNull();
            RuleFor(x => x.TrackNumber).NotEmpty().NotNull();
            RuleFor(x =>x.AddressFromId).NotEmpty().NotNull();
            RuleFor(x =>x.AddressToId).NotEmpty().NotNull();

        }
    }

    /// <summary>
    /// Валидатор для <see cref="UpdateOrderRequest"/>.<br/>
    /// </summary>

    public class UpdateOrderValidator : AbstractValidator<UpdateOrderRequest>
    {
        /// <summary>
        /// Валидация модели добавления заказа
        /// </summary>
        public UpdateOrderValidator()
        {
            RuleFor(x => x.Id).NotEmpty().NotNull();
            RuleFor(x => x.Weight).NotEmpty().NotNull();
            RuleFor(x => x.TrackNumber).NotEmpty().NotNull();
            RuleFor(x => x.AddressFromId).NotEmpty().NotNull();
            RuleFor(x => x.AddressToId).NotEmpty().NotNull();

        }
    }
}
