﻿using FastLogistic.Models.Seller;
using FluentValidation;

namespace FastLogistic.Validators
{
    /// <summary>
    /// Валидатор для <see cref="AddSellerRequest"/>.<br/>
    /// </summary>

    public class AddSellerValidator : AbstractValidator<AddSellerRequest>
    {
        /// <summary>
        /// Валидация модели добавления Продавца 
        /// </summary>
        public AddSellerValidator()
        {
            RuleFor(x => x.Name).NotEmpty().NotNull().MaximumLength(64);
            RuleFor(x => x.UserId).NotEmpty().NotNull();
        }
    }

    /// <summary>
    /// Валидатор для <see cref="UpdateSellerRequest"/>.<br/>
    /// </summary>
    public class UpdateSellerValidator : AbstractValidator<UpdateSellerRequest>
    {
        /// <summary>
        /// Валидация модели обновления Продавца 
        /// </summary>
        public UpdateSellerValidator()
        {
            RuleFor(x => x.Id).NotEmpty().NotNull();
            RuleFor(x => x.Name).NotEmpty().NotNull().MaximumLength(64);
        }
    }

}
