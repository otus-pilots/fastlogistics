﻿using FastLogistic.Models.Address;
using FastLogistic.Models.Zone;
using FluentValidation;

namespace FastLogistic.Validators
{
    /// <summary>
    /// Валидатор для <see cref="AddZoneRequest"/>.<br/>
    /// </summary>

    public class AddZoneValidator : AbstractValidator<AddZoneRequest>
    {
        /// <summary>
        /// Валидация модели добавления зоны адресов. 
        /// </summary>
        public AddZoneValidator()
        {
            RuleFor(x => x.Name).NotEmpty().NotNull().MaximumLength(128);
            RuleFor(x => x.CostRate).NotEmpty().NotNull().LessThan(0).WithMessage("CostRate должна быть больше 0");
            RuleFor(x => x.TimeRate).NotEmpty().NotNull().LessThan(0).WithMessage("TimeRate должна быть больше 0");
        }
    }

    /// <summary>
    /// Валидатор для <see cref="UpdateZoneRequest"/>.<br/>
    /// </summary>
    public class UpdateZoneValidator : AbstractValidator<UpdateZoneRequest>
    {
        /// <summary>
        /// Валидация модели обновления зоны адресов. 
        /// </summary>
        public UpdateZoneValidator()
        {
            RuleFor(x => x.Name).NotEmpty().NotNull().MaximumLength(128);
            RuleFor(x => x.CostRate).NotEmpty().NotNull().LessThan(0).WithMessage("CostRate должна быть больше 0");
            RuleFor(x => x.TimeRate).NotEmpty().NotNull().LessThan(0).WithMessage("TimeRate должна быть больше 0");
        }
    }

}
