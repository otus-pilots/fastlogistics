﻿using FastLogistic.Models.Zone;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FastLogistic.Services
{
    /// <summary>
    /// Сервис работы с сущностью Zone.
    /// </summary>
    public interface IZoneService
    {
        /// <summary>
        ///     Получить список всех зон доставки
        /// </summary>
        /// <returns></returns>
        Task<List<ZoneResponse>> GetZoneAsync();

        /// <summary>
        ///     Получить данные о зоне доставки по ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ZoneResponse> GetZoneByIdAsync(int id);

        /// <summary>
        /// Добавление новой зоны доставки
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<int> CreateZoneAsync(AddZoneRequest request);

        /// <summary>
        /// Обновление информации о зоне доставки
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<int?> UpdateZoneAsync(UpdateZoneRequest request);

        /// <summary>
        ///     Удаление зоны доставки
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<int?> DeleteZoneAsync(int id);

    }
}
