﻿using FastLogistic.Models.Courier;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FastLogistic.Services
{
    /// <summary>
    /// Сервис работы с сущностью курьер.
    /// </summary>
    public interface ICourierService
    {
        /// <summary>
        /// Получить список всех курьеров
        /// </summary>
        Task<List<CourierShortResponse>> GetCouriersAsync();

        /// <summary>
        /// Получить данные курьера по id
        /// </summary>
        /// <param name="id">Id курьера.</param>
        /// <returns></returns>
        Task<CourierResponse> GetCourierByIdAsync(int id);

        /// <summary>
        /// Добавление нового курьера
        /// </summary>
        /// <param name="request">Новые данные курьера</param>
        /// <returns></returns>
        Task<int> AddCourierAsync(AddCourierRequest request);

        /// <summary>
        /// Обновить данные о курьере
        /// </summary>
        /// <param name="request">Новые данные курьера</param>
        /// <returns></returns>
        Task<int?> UpdateCourierAsync(UpdateCourierRequest request);

        /// <summary>
        /// Удаление курьера
        /// </summary>
        /// <param name="id">Id курьера.</param>
        /// <returns></returns>
        Task<int?> DeleteCourierAsync(int id);
    }
}
