﻿using FastLogistic.Models.Order;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FastLogistic.Services
{
    /// <summary>
    /// Сервис работы с сущностью заказ.
    /// </summary>
    public interface IOrderService
    {
        /// <summary>
        ///     Получение списка заказов.
        /// </summary>
        /// <returns></returns>
        Task<List<OrderShortResponse>> GetOrdersAsync();

        /// <summary>
        ///     Получение фильтрованного списка заказов
        /// </summary>
        /// <param name="request">Условие фильтрации.</param>
        /// <returns></returns>
        Task<List<OrderResponse>> GetOrdersFilteredAsync(GetOrdersFilterRequest request = null);

        /// <summary>
        ///     Возвращает информацию о заказе.
        /// </summary>
        /// <param name="id">Id заказа</param>
        /// <returns></returns>
        Task<OrderResponse> GetOrderByIdAsync(int id);

        /// <summary>
        ///     Возвращает информацию о заказе по трекномеру.
        /// </summary>
        /// <param name="trackNubmer"></param>
        /// <returns></returns>
        Task<OrderResponse> GetOrderByTrackAsync(Int64 trackNubmer);

        /// <summary>
        ///     Добавление нового заказа.
        /// </summary>
        /// <param name="request">Новый заказ.</param>
        /// <returns></returns>
        Task<int?> CreateOrderAsync(AddOrderRequest request);

        /// <summary>
        ///     Обновление информации о заказе.
        /// </summary>
        /// <param name="request">Новая информация о заказе.</param>
        /// <returns></returns>
        Task<int?> UpdateOrderAsync(UpdateOrderRequest request);

        /// <summary>
        ///     Удаление заказа
        /// </summary>
        /// <param name="id">Id заказа</param>
        /// <returns></returns>
        Task<int?> DeleteOrderAsync(int id);

        /// <summary>
        ///     Назначение курьера на заказ
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<int?> AssignCourierToOrderAsync(int orderId, int userId);
    }
}
