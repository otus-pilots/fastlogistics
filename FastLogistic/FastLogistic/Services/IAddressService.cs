﻿using FastLogistic.Models.Address;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FastLogistic.Services
{
    public interface IAddressService
    {
        /// <summary>
        /// Получить список всех адресов
        /// </summary>
        /// <returns></returns>
        Task<List<AddressResponse>> GetAddressesAsync();

        /// <summary>
        /// Получить данные об адресе по ID
        /// </summary>
        /// <param name="id">Id адреса</param>
        /// <returns></returns>
        Task<AddressResponse> GetAddressByIdAsync(int id);

        /// <summary>
        /// Добавить новый адрес
        /// </summary>
        /// <param name="request">Данные адреса</param>
        /// <returns></returns>
        Task<long> AddAdressAsync(AddAddressRequest request);

        /// <summary>
        /// Обновить данные об адресе
        /// </summary>
        /// <param name="request">Данные адреса</param>
        /// <returns></returns>
        Task<int?> UpdateAddressAsync(UpdateAddressRequest request);

        /// <summary>
        /// Удалить адрес
        /// </summary>
        /// <param name="id">id адреса</param>
        /// <returns></returns>
        Task<int?> DeleteAddressAsync(int id);

    }
}