﻿using FastLogistic.Core.Abstractions.Repositories;
using FastLogistic.Core.Domain;
using FastLogistic.Mappers;
using FastLogistic.Models.Order;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Services
{
    /// <summary>
    /// Сервис работы с сущностью заказы.
    /// </summary>
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IRepository<Status> _statusRepository;
        private readonly ISellerRepository _sellerRepository;
        private readonly ICourierRepository _courierRepository;
        private readonly ILogger<OrderService> _logger;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="orderRepository"></param>
        /// <param name="statusRepository"></param>
        /// <param name="sellerRepository"></param>
        /// <param name="courierRepository"></param>
        /// <param name="logger"></param>
        public OrderService(
            IOrderRepository orderRepository,
            IRepository<Status> statusRepository,
            ILogger<OrderService> logger, 
            ISellerRepository sellerRepository, 
            ICourierRepository courierRepository)
        {
            _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
            _statusRepository = statusRepository ?? throw new ArgumentNullException(nameof(statusRepository));
            _sellerRepository = sellerRepository;
            _courierRepository = courierRepository;
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        ///<inheritdoc/>
        public async Task<List<OrderShortResponse>> GetOrdersAsync()
        {
            var orders = await _orderRepository.GetAllAsync();

            var orderModelList = orders
                .Select(x => OrderMapper.MapFromModelToShortResponse(x))
                .ToList();

            return orderModelList;
        }

        ///<inheritdoc/>
        public async Task<List<OrderResponse>> GetOrdersFilteredAsync(GetOrdersFilterRequest request = null)
        {
            var orderFilter = OrderMapper.MapFromFilterRequest(request);

            var orders = await _orderRepository.GetAllFilteredAsync(orderFilter);

            var orderModelList = orders
                .Select(x => OrderMapper.MapFromModelToResponse(x))
                .ToList();

            return orderModelList;
        }

        ///<inheritdoc/>
        public async Task<OrderResponse> GetOrderByIdAsync(int id)
        {
            var order = await _orderRepository.GetByIdAsync(id);

            if (order == null)
                return null;

            var orderModel = OrderMapper.MapFromModelToResponse(order);

            return orderModel;
        }

        ///<inheritdoc/>
        public async Task<OrderResponse> GetOrderByTrackAsync(Int64 trackNubmer)
        {
            var order = await _orderRepository.GetOrderByTrackAsync(trackNubmer);

            if (order == null)
                return null;

            var orderModel = OrderMapper.MapFromModelToResponse(order);

            return orderModel;
        }

        ///<inheritdoc/>
        public async Task<int?> CreateOrderAsync(AddOrderRequest request)
        {
            var status = await _statusRepository.GetByIdAsync(request.StatusId);
            if (status is null)
                return null;

            var seller = await _sellerRepository.GetByIdAsync(request.SellerId);
            if (seller is null)
                return null;

            var order = OrderMapper.MapFromAddRequestToModel(request);

            var id = await _orderRepository.AddAsync(order);

            return id;
        }

        ///<inheritdoc/>
        public async Task<int?> UpdateOrderAsync(UpdateOrderRequest request)
        {
            var order = await _orderRepository.GetByIdAsync(request.Id);

            if (order is null)
                return null;

            OrderMapper.MapFromUpdateRequest(ref order, request);

            await _orderRepository.UpdateAsync(order);
            return request.Id;
        }

        ///<inheritdoc/>
        public async Task<int?> DeleteOrderAsync(int id)
        {
            var order = await _orderRepository.GetByIdAsync(id);

            if (order is null)
                return null;

            await _orderRepository.DeleteAsync(order);

            return id;
        }

        public async Task<int?> AssignCourierToOrderAsync(int orderId, int userId)
        {
            var order = await _orderRepository.GetByIdAsync(orderId);
            if (order is null)
                return null;

            var courier = await _courierRepository.GetCourierIdByUserIdAsync(userId);
            if (courier is null)
                return null;

            order.CourierId = courier.Id;

            await _orderRepository.UpdateAsync(order);
            
            return orderId;
        }
    }
}
