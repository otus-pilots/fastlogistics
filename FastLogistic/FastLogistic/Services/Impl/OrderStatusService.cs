﻿using FastLogistic.Core.Abstractions.Repositories;
using FastLogistic.Core.Domain;
using FastLogistic.Mappers;
using FastLogistic.Models.OrderStatus;
using FastLogistic.Notifications;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Services
{
    /// <summary>
    /// Сервис работы с сущностью OrderStatus.
    /// </summary>
    public class OrderStatusService : IOrderStatusService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly INotificationManager _notificationManager;
        private readonly IRepository<Status> _statusRepository;
        private readonly ILogger<OrderStatusService> _logger;

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="orderRepository"></param>
        /// <param name="notificationManager"></param>
        /// <param name="statusRepository"></param>
        /// <param name="logger"></param>
        public OrderStatusService(
            IOrderRepository orderRepository,
            INotificationManager notificationManager,
            IRepository<Status> statusRepository,
            ILogger<OrderStatusService> logger)
        {
            _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
            _notificationManager = notificationManager ?? throw new ArgumentNullException(nameof(notificationManager));
            _statusRepository = statusRepository ?? throw new ArgumentNullException(nameof(statusRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        ///<inheritdoc/>
        public async Task<List<OrderStatusResponse>> GetOrderStatusesByIdAsync(int id)
        {
            var orderStatuses = await _orderRepository.GetOrderStatusesByIdAsync(id);
            if (orderStatuses is null)
                return null;

            var orderStatusesModelList = orderStatuses
                .Select(OrderStatusMapper.MapFromModelToResponse)
                .ToList();

            return orderStatusesModelList;
        }

        ///<inheritdoc/>
        public async Task<Order> AddOrderStatusAsync(AddOrderStatusRequest request)
        {

            var order = await _orderRepository.GetByIdAsync(request.OrderId);
            if (order is null)
                return null;

            var status = await _statusRepository.GetByIdAsync(request.StatusId);
            if (status is null)
                return null;

            order.Statuses.Add(status);

            await _orderRepository.UpdateAsync(order);

            //Отправляем в раббит сообщение о смене статуса
            _notificationManager.Send(order);
            return order;
        }



    }
}
