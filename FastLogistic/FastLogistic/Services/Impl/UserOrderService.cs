﻿using FastLogistic.Core.Abstractions.Repositories;
using FastLogistic.Core.Domain;
using FastLogistic.Models.UserOrder;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Services.Impl
{
    public class UserOrderService : IUserOrderService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IRepository<User> _userRepository;
        private readonly ILogger<UserOrderService> _logger;

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="orderRepository"></param>
        /// <param name="userRepository"></param>
        /// <param name="logger"></param>
        public UserOrderService(
            IOrderRepository orderRepository,
            IRepository<User> userRepository,
            ILogger<UserOrderService> logger)
        {
            _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        ///<inheritdoc/>
        public async Task<Boolean> AddUserOrderAsync(AddUserOrderRequest request)
        {

            var order = await _orderRepository.GetByIdAsync(request.OrderId);
            if (order is null)
                return false;

            var user = await _userRepository.GetByIdAsync(request.UserId);
            if (user is null)
                return false;

            user.Orders.Add(order);

            await _userRepository.UpdateAsync(user);

            return true;
        }


    }
}
