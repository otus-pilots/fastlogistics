﻿using FastLogistic.Core.Abstractions.Repositories;
using FastLogistic.Core.Domain;
using FastLogistic.Mappers;
using FastLogistic.Models.Zone;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Services.Impl
{
    /// <summary>
    /// Сервис работы с сущностью Zone.
    /// </summary>
    public class ZoneService : IZoneService
    {
        private readonly IRepository<Zone> _zoneRepository;
        private readonly ILogger<ZoneService> _logger;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="zoneRepository"></param>
        /// <param name="logger"></param>
        public ZoneService(
            IRepository<Zone> zoneRepository,
            ILogger<ZoneService> logger
            )
        {
            _zoneRepository = zoneRepository ?? throw new ArgumentNullException(nameof(zoneRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        ///<inheritdoc/>
        public async Task<List<ZoneResponse>> GetZoneAsync()
        {
            var zones = await _zoneRepository.GetAllAsync();

            var zonesModelList = zones.Select(zone =>
                ZoneMapper.MapFromModelToResponse(zone)).ToList();

            return zonesModelList;
        }

        ///<inheritdoc/>
        public async Task<ZoneResponse> GetZoneByIdAsync(int id)
        {
            var zone = await _zoneRepository.GetByIdAsync(id);

            if (zone == null)
                return null;

            var response = ZoneMapper.MapFromModelToResponse(zone);

            return response;
        }

        ///<inheritdoc/>
        public async Task<int> CreateZoneAsync(AddZoneRequest request)
        {

            var address = ZoneMapper.MapFromAddRequestToModel(request);

            var id = await _zoneRepository.AddAsync(address);

            return id;
        }

        ///<inheritdoc/>
        public async Task<int?> UpdateZoneAsync(UpdateZoneRequest request)
        {
            var zone = await _zoneRepository.GetByIdAsync(request.Id);

            if (zone is null)
                return null;

            ZoneMapper.MapFromUpdateRequest(ref zone, request);

            await _zoneRepository.UpdateAsync(zone);
            return request.Id;
        }

        ///<inheritdoc/>
        public async Task<int?> DeleteZoneAsync(int id)
        {
            var zone = await _zoneRepository.GetByIdAsync(id);

            if (zone is null)
                return null;

            await _zoneRepository.DeleteAsync(zone);

            return id;
        }

    }
}
