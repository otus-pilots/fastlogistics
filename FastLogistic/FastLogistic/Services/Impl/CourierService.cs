﻿using FastLogistic.Core.Abstractions.Repositories;
using FastLogistic.Core.Domain;
using FastLogistic.Mappers;
using FastLogistic.Models.Courier;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Services.Impl
{
    /// <summary>
    /// Сервис работы с сущностью курьер.
    /// </summary>
    public class CourierService : ICourierService
    {
        private readonly IRepository<Courier> _courierRepository;
        private readonly ILogger<CourierService> _logger;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="courierRepository"></param>
        /// <param name="logger"></param>
        public CourierService(
            IRepository<Courier> courierRepository,
            ILogger<CourierService> logger
            )
        {
            _courierRepository = courierRepository ?? throw new ArgumentNullException(nameof(courierRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        ///<inheritdoc/>
        public async Task<List<CourierShortResponse>> GetCouriersAsync()
        {
            var couriers = await _courierRepository.GetAllAsync();

            var couriersModelList = couriers
                .Select(x => CourierMapper.MapFromModelToShortResponse(x))
                .ToList();

            return couriersModelList;
        }

        ///<inheritdoc/>
        public async Task<CourierResponse> GetCourierByIdAsync(int id)
        {
            var courier = await _courierRepository.GetByIdAsync(id);

            if (courier == null)
                return null;

            var courierModel = CourierMapper.MapFromModelToResponse(courier);

            return courierModel;
        }

        ///<inheritdoc/>
        public async Task<int> AddCourierAsync(AddCourierRequest request)
        {
            var courier = CourierMapper.MapFromAddRequestToModel(request, new List<Zone>());

            var id = await _courierRepository.AddAsync(courier);

            return id;
        }

        ///<inheritdoc/>
        public async Task<int?> UpdateCourierAsync(UpdateCourierRequest request)
        {
            var courier = await _courierRepository.GetByIdAsync(request.Id.Value);

            if (courier is null)
                return null;

            CourierMapper.MapFromUpdateRequest(courier, request, new List<Zone>());

            await _courierRepository.UpdateAsync(courier);

            return request.Id;
        }

        ///<inheritdoc/>
        public async Task<int?> DeleteCourierAsync(int id)
        {
            var courier = await _courierRepository.GetByIdAsync(id);

            if (courier is null)
                return null;

            await _courierRepository.DeleteAsync(courier);

            return id;
        }


    }
}
