using FastLogistic.Core.Abstractions.Repositories;
using FastLogistic.Core.Domain;
using FastLogistic.Mappers;
using FastLogistic.Models.Order;
using FastLogistic.Models.User;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Services.Impl
{
    /// <summary>
    /// Сервис работы с сущностью User.
    /// </summary>
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly ILogger<UserService> _logger;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="userRepository"></param>
        /// <param name="logger"></param>
        public UserService(
            IUserRepository userRepository,
            ILogger<UserService> logger
            )
        {
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        ///<inheritdoc/>
        public async Task<List<UserShortResponse>> GetUsersAsync()
        {
            var users = await _userRepository.GetAllAsync();

            var usersModelList = users.Select(u =>
                UserMapper.MapFromModelToShortResponse(u)).ToList();

            return usersModelList;
        }

        ///<inheritdoc/>
        public async Task<UserResponse> GetUserAsync(int id)
        {
            var user = await _userRepository.GetByIdAsync(id);

            if (user is null)
                return null;

            var userModel = UserMapper.MapFromModelToResponse(user);

            return userModel;
        }

        ///<inheritdoc/>
        public async Task<List<OrderShortResponse>> GetUserOrdersByIdAsync(int id)
        {
            var user = await _userRepository.GetUserOrdersByIdAsync(id);

            if (user is null)
                return null;

            var orderModelList = user.Orders
                .Select(x => OrderMapper.MapFromModelToShortResponse(x))
                .ToList();

            return orderModelList;
        }


        ///<inheritdoc/>
        public async Task<int> AddUserAsync(AddUserRequest userRequest)
        {
            var user = UserMapper.MapFromAddRequestToModel(userRequest);
            //По умолчанию при добавлении пользователя проставляется роль Registered
            user.RoleId = (int)RoleStatics.Registered;

            var id = await _userRepository.AddAsync(user);

            return id;
        }

        ///<inheritdoc/>
        public async Task<int?> UpdateUserInfoAsync(UpdateUserInfoRequest userRequest)
        {

            var user = await _userRepository.GetByIdAsync(userRequest.Id);

            if (user is null)
                return null;

            UserMapper.MapFromUpdateInfoRequest(user, userRequest);

            await _userRepository.UpdateAsync(user);

            return userRequest.Id;
        }

        ///<inheritdoc/>
        public async Task<int?> UpdateUserRoleAsync(UpdateUserRoleRequest userRequest)
        {

            var user = await _userRepository.GetByIdAsync(userRequest.Id);

            if (user is null)
                return null;

            UserMapper.MapFromUpdateRoleRequest(user, userRequest);

            await _userRepository.UpdateAsync(user);

            return userRequest.Id;
        }

        ///<inheritdoc/>
        public async Task<int?> DeleteUserAsync(int id)
        {
            var user = await _userRepository.GetByIdAsync(id);

            if (user is null)
                return null;

            await _userRepository.DeleteAsync(user);

            return id;

        }
    }
}
