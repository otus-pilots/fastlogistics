﻿using FastLogistic.Core.Abstractions.Repositories;
using FastLogistic.Core.Domain;
using FastLogistic.Mappers;
using FastLogistic.Models.Seller;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Services.Impl
{
    /// <summary>
    /// Сервис работы с сущностью Seller.
    /// </summary>
    public class SellerService : ISellerService
    {
        private readonly IRepository<Seller> _sellerRepository;
        private readonly ILogger<SellerService> _logger;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="sellerRepository"></param>
        /// <param name="logger"></param>
        public SellerService(
            IRepository<Seller> sellerRepository,
            ILogger<SellerService> logger
            )
        {
            _sellerRepository = sellerRepository ?? throw new ArgumentNullException(nameof(sellerRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        ///<inheritdoc/>
        public async Task<List<SellerShortResponse>> GetSellersAsync()
        {
            var sellers = await _sellerRepository.GetAllAsync();

            var sellerModelList = sellers.Select(s =>
                SellerMapper.MapFromModelToShortResponse(s)).ToList();

            return sellerModelList;
        }

        ///<inheritdoc/>
        public async Task<SellerResponse> GetSellerAsync(int id)
        {
            var seller = await _sellerRepository.GetByIdAsync(id);

            if (seller is null)
                return null;

            var sellerModel = SellerMapper.MapFromModelToResponse(seller);

            return sellerModel;
        }

        ///<inheritdoc/>
        public async Task<int> AddSellerAsync(AddSellerRequest sellerRequest)
        {

            var seller = SellerMapper.MapFromAddRequestToModel(sellerRequest);

            var id = await _sellerRepository.AddAsync(seller);

            return  id ;
        }

        ///<inheritdoc/>
        public async Task<int?> UpdateSellerAsync(UpdateSellerRequest sellerRequest)
        {

            var seller = await _sellerRepository.GetByIdAsync(sellerRequest.Id);

            if (seller is null)
                return null;

            SellerMapper.MapFromUpdateRequest(seller, sellerRequest);

            await _sellerRepository.UpdateAsync(seller);

            return sellerRequest.Id;
        }

        ///<inheritdoc/>
        public async Task<int?> DeleteSellerAsync(int id)
        {
            var seller = await _sellerRepository.GetByIdAsync(id);

            if (seller is null)
                return null;

            await _sellerRepository.DeleteAsync(seller);

            return id;
        }


    }
}
