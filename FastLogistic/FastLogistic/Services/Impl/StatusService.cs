﻿using FastLogistic.Core.Abstractions.Repositories;
using FastLogistic.Core.Domain;
using FastLogistic.Mappers;
using FastLogistic.Models.Statuses;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Services.Impl
{
    /// <summary>
    /// Сервис работы с сущностью Status.
    /// </summary>
    public class StatusService : IStatusService
    {
        private readonly IRepository<Status> _statusRepository;
        private readonly ILogger<StatusService> _logger;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="statusRepository"></param>
        /// <param name="logger"></param>
        public StatusService(
            IRepository<Status> statusRepository,
            ILogger<StatusService> logger
            )
        {
            _statusRepository = statusRepository ?? throw new ArgumentNullException(nameof(statusRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        ///<inheritdoc/>
        public async Task<List<StatusResponse>> GetStatusesAsync()
        {
            var statuses = await _statusRepository.GetAllAsync();

            var statusesModelList = statuses
                .OrderBy(s => s.Id)
                .Select(StatusMapper.MapFromModelToResponse)
                .ToList();

            return statusesModelList;
        }

    }
}
