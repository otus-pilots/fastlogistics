﻿using FastLogistic.Core.Abstractions.Repositories;
using FastLogistic.Core.Domain;
using FastLogistic.Mappers;
using FastLogistic.Models.Address;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Services.Impl
{
    /// <summary>
    /// Сервис работы с адресом.
    /// </summary>
    public class AddressService : IAddressService
    {
        private readonly IRepository<Address> _addressRepository;
        private readonly ILogger<AddressService> _logger;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="addressRepository"></param>
        /// <param name="logger"></param>
        public AddressService(
            IRepository<Address> addressRepository,
            ILogger<AddressService> logger
            )
        {
            _addressRepository = addressRepository ?? throw new ArgumentNullException(nameof(addressRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        /// <inheritdoc/>
        public async Task<List<AddressResponse>> GetAddressesAsync()
        {
            var addresses = await _addressRepository.GetAllAsync();

            var addressesModelList = addresses.Select(address =>
                AddressMapper.MapFromModelToResponse(address)).ToList();

            return addressesModelList;
        }

        /// <inheritdoc/>
        public async Task<AddressResponse> GetAddressByIdAsync(int id)
        {
            var address = await _addressRepository.GetByIdAsync(id);

            if (address == null)
                return null;

            var response = AddressMapper.MapFromModelToResponse(address);

            return response;
        }

        /// <inheritdoc/>
        public async Task<Int64> AddAdressAsync(AddAddressRequest request)
        {
            var address = AddressMapper.MapFromAddRequestToModel(request);

            var id = await _addressRepository.AddAsync(address);

            return id;
        }

        /// <inheritdoc/>
        public async Task<int?> UpdateAddressAsync(UpdateAddressRequest request)
        {
            var address = await _addressRepository.GetByIdAsync(request.Id);

            if (address is null)
                return null;

            AddressMapper.MapFromUpdateRequest(address, request);

            await _addressRepository.UpdateAsync(address);

            return request.Id;
        }

        /// <inheritdoc/>
        public async Task<int?> DeleteAddressAsync(int id)
        {
            var address = await _addressRepository.GetByIdAsync(id);

            if (address is null)
                return null;

            await _addressRepository.DeleteAsync(address);

            return id;
        }



    }
}
