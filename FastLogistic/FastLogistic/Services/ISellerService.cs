﻿using FastLogistic.Models.Seller;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FastLogistic.Services
{
    /// <summary>
    /// Сервис работы с сущностью Seller.
    /// </summary>
    public interface ISellerService
    {
        /// <summary>
        ///     Получение списка продавцов
        /// </summary>
        /// <returns></returns>
        Task<List<SellerShortResponse>> GetSellersAsync();

        /// <summary>
        ///     Получение подробной информации о продавце по Id
        /// </summary>
        /// <param name="id">Id продавца.</param>
        /// <returns></returns>
        Task<SellerResponse> GetSellerAsync(int id);

        /// <summary>
        ///     Добавление (регистрация) нового продавца
        /// </summary>
        /// <param name="sellerRequest">Новый продавец.</param>
        /// <returns></returns>
        Task<int> AddSellerAsync(AddSellerRequest sellerRequest);

        /// <summary>
        ///     Обновление данных о продавце
        /// </summary>
        /// <param name="sellerRequest"></param>
        /// <returns></returns>
        Task<int?> UpdateSellerAsync(UpdateSellerRequest sellerRequest);

        /// <summary>
        ///     Удаление продавца
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<int?> DeleteSellerAsync(int id);

    }
}
