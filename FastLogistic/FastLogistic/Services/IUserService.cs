﻿using FastLogistic.Models.Order;
using FastLogistic.Models.User;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FastLogistic.Services
{
    /// <summary>
    /// Сервис работы с сущностью User.
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        ///     Получение списка пользователей
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<UserResponse> GetUserAsync(int id);

        /// <summary>
        ///     Получение подробной информации о пользователе по Id
        /// </summary>
        /// <returns></returns>
        Task<List<UserShortResponse>> GetUsersAsync();

        /// <summary>
        /// Возвращает Список привязанных заказов.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<List<OrderShortResponse>> GetUserOrdersByIdAsync(int id);

        /// <summary>
        ///     Добавление (регистрация) нового пользователя
        /// </summary>
        /// <param name="userRequest"></param>
        /// <returns></returns>
        Task<int> AddUserAsync(AddUserRequest userRequest);

        /// <summary>
        ///     Изменение информации о пользователе.
        /// </summary>
        /// <param name="userRequest"></param>
        /// <returns></returns>
        Task<int?> UpdateUserInfoAsync(UpdateUserInfoRequest userRequest);

        /// <summary>
        ///     Изменение роли пользователя.
        /// </summary>
        /// <param name="userRequest"></param>
        /// <returns></returns>
        Task<int?> UpdateUserRoleAsync(UpdateUserRoleRequest userRequest);

        /// <summary>
        ///     Удаление пользователя
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<int?> DeleteUserAsync(int id);
    }
}
