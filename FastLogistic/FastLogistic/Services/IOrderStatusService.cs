﻿using FastLogistic.Core.Domain;
using FastLogistic.Models.OrderStatus;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FastLogistic.Services
{
    /// <summary>
    /// Сервис работы с сущностью OrderStatus.
    /// </summary>
    public interface IOrderStatusService
    {
        /// <summary>
        ///     Получение истории статусов заказа по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<List<OrderStatusResponse>> GetOrderStatusesByIdAsync(int id);

        /// <summary>
        ///     Изменение статуса заказа (добавление новой записи OrderStatus)
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<Order> AddOrderStatusAsync(AddOrderStatusRequest request);
    }
}
