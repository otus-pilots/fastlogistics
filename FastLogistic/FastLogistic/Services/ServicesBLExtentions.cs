﻿using FastLogistic.Services.Impl;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Services
{
    /// <summary>
    ///     Расширение для добавления сервисов бизнес логики
    /// </summary>
    public static class ServicesBLExtentions
    {
        /// <summary>
        /// Добаляет сервисы бизнес логики.
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddServicesBL(this IServiceCollection services)
        {
            return services
                .AddScoped<IAddressService, AddressService>()
                .AddScoped<ICourierService, CourierService>()
                .AddScoped<IOrderService, OrderService>()
                .AddScoped<IOrderStatusService, OrderStatusService>()
                .AddScoped<ISellerService, SellerService>()
                .AddScoped<IStatusService, StatusService>()
                .AddScoped<IUserService, UserService>()
                .AddScoped<IZoneService, ZoneService>()
                .AddScoped<IUserOrderService, UserOrderService>()
                ;
        }

    }
}
