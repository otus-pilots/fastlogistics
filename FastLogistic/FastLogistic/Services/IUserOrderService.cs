﻿using FastLogistic.Models.UserOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FastLogistic.Services
{
    /// <summary>
    /// Сервис работы со связями User Order.
    /// </summary>
    public interface IUserOrderService
    {
        /// <summary>
        /// Добавляет связь пользователя с заказом.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<Boolean> AddUserOrderAsync(AddUserOrderRequest request);
    }
}
