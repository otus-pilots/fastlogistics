﻿using FastLogistic.Models.Statuses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FastLogistic.Services
{
    /// <summary>
    /// Сервис работы с сущностью Status.
    /// </summary>
    public interface IStatusService
    {
        /// <summary>
        ///     Получение списка статусов
        /// </summary>
        /// <returns></returns>
        Task<List<StatusResponse>> GetStatusesAsync();
    }
}
