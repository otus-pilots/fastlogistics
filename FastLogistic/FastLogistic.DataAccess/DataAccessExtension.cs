using FastLogistic.Core.Abstractions.Repositories;
using FastLogistic.DataAccess.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace FastLogistic.DataAccess
{
    /// <summary>
    ///     Расширение для добавления dbcontext
    /// </summary>
    public static class DataAccessExtension
    {
        public static IServiceCollection AddDb(this IServiceCollection services, string connectionString)
        {
            return services
                .AddDbContext<DataContext>(options => options.UseNpgsql(connectionString))
                .AddScoped(typeof(IRepository<>), typeof(EfRepository<>))
                .AddScoped<IUserRepository, UserRepository>()
                .AddScoped<ICourierRepository, CourierRepository>()
                .AddScoped<ISellerRepository, SellerRepository>()
                .AddScoped<IOrderRepository, OrderRepository>()
                .AddScoped<IDistributedCacheRepository, DistributedCacheRepository>();
        }
    }
}