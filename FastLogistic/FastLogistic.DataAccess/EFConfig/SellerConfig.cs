using FastLogistic.Core.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FastLogistic.DataAccess.EFConfig
{
    public class SellerConfig : IEntityTypeConfiguration<Seller>
    {
        public void Configure(EntityTypeBuilder<Seller> builder)
        {
            builder
                .HasKey(s => s.Id);

            builder
                .Property(s => s.Id)
                .HasIdentityOptions(100);
            
            builder
                .Property(s => s.Name)
                .HasMaxLength(64);
        }
    }
}