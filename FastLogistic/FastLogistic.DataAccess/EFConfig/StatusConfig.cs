using FastLogistic.Core.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FastLogistic.DataAccess.EFConfig
{
    public class StatusConfig : IEntityTypeConfiguration<Status>
    {
        public void Configure(EntityTypeBuilder<Status> builder)
        {
            builder.HasKey(s => s.Id);

            builder
                .Property(s => s.Id)
                .HasIdentityOptions(100);
            builder.Property(s => s.Name).HasMaxLength(64);

            builder.HasMany(o => o.Orders)
                .WithMany(s => s.Statuses)
                .UsingEntity<OrderStatus>(
                    j => j
                        .HasOne(pt => pt.Order)
                        .WithMany(t => t.OrderStatuses)
                        .HasForeignKey(pt => pt.OrderId),
                    j => j
                        .HasOne(pt => pt.Status)
                        .WithMany(p => p.OrderStatuses)
                        .HasForeignKey(pt => pt.StatusId),
                    j =>
                    {
                        j.Property(pt => pt.TimeChanged).HasDefaultValueSql("CURRENT_TIMESTAMP");
                        j.HasKey(t => new {  t.StatusId, t.OrderId, t.TimeChanged });
                    });
        }
    }
}