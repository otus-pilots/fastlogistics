﻿using FastLogistic.Core.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FastLogistic.DataAccess.EFConfig
{
    public class AddressConfig : IEntityTypeConfiguration<Address>
    {
        public void Configure(EntityTypeBuilder<Address> builder)
        {
            builder
                .HasKey(address => address.Id);

            builder
                .Property(address => address.Id)
                .HasIdentityOptions(100);
            builder
                .Property(address => address.RegistrationAddress)
                .HasMaxLength(254);

            builder
                .HasOne(address => address.Zone)
                .WithMany(zone => zone.Addresses)
                .HasForeignKey(address => address.ZoneId);
        }
    }
}