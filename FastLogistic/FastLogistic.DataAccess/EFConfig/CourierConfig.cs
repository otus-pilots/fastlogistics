using FastLogistic.Core.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FastLogistic.DataAccess.EFConfig
{
    public class CourierConfig : IEntityTypeConfiguration<Courier>
    {
        public void Configure(EntityTypeBuilder<Courier> builder)
        {
            builder
                .HasKey(c => c.Id);

            builder
                .Property(c => c.Id)
                .HasIdentityOptions(100);

            builder
                .HasMany(c => c.Zones)
                .WithMany(z => z.Couriers)
                .UsingEntity<CourierZone>(
                    czr => czr
                        .HasOne(cz => cz.Zone)
                        .WithMany(z => z.CourierZones)
                        .HasForeignKey(cz => cz.ZoneId),
                    czl => czl
                        .HasOne(cz => cz.Courier)
                        .WithMany(c => c.CourierZones)
                        .HasForeignKey(cz => cz.CourierId),
                    czj =>
                    {
                        czj.HasKey(cz => new
                        {
                            cz.CourierId,
                            cz.ZoneId
                        });
                    });
        }
    }
}