using FastLogistic.Core.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FastLogistic.DataAccess.EFConfig
{
    public class UserConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder
                .HasKey(u => u.Id);
            builder
                .HasIndex(u => u.Email)
                .IsUnique();

            builder
                .Property(u => u.Id)
                .HasIdentityOptions(100);
            builder
                .Property(u => u.Login)
                .HasMaxLength(64);
            builder
                .Property(u => u.Password)
                .HasMaxLength(64);
            builder
                .Property(u => u.Email)
                .HasMaxLength(254);
            builder
                .Property(u => u.PhoneNumber)
                .HasMaxLength(12);

            builder
                .HasOne(u => u.Role)
                .WithMany(r => r.Users)
                .HasForeignKey(u => u.RoleId);

            builder
                .HasOne(u => u.Seller)
                .WithOne(s => s.User)
                .HasForeignKey<Seller>(s => s.UserId)
                .OnDelete(DeleteBehavior.SetNull);

            builder
                .HasOne(u => u.Courier)
                .WithOne(c => c.User)
                .HasForeignKey<Courier>(c => c.UserId)
                .OnDelete(DeleteBehavior.SetNull);

            builder.HasMany(o => o.Orders)
                .WithMany(s => s.Users)
                .UsingEntity<UserOrders>(
                    j => j
                        .HasOne(pt => pt.Order)
                        .WithMany(t => t.UserOrders)
                        .HasForeignKey(pt => pt.OrderId),
                    j => j
                        .HasOne(pt => pt.User)
                        .WithMany(p => p.UserOrders)
                        .HasForeignKey(pt => pt.UserId),
                    j =>
                    {
                        j.HasKey(t => new { t.UserId, t.OrderId, t.IsActive });
                    });
        }
    }
}