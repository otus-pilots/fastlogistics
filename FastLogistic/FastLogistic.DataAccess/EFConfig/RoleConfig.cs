using FastLogistic.Core.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FastLogistic.DataAccess.EFConfig
{
    public class RoleConfig : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder
                .HasKey(r => r.Id);

            builder
                .Property(r => r.Id)
                .HasIdentityOptions(100);
            
            builder
                .Property(r => r.Name)
                .HasMaxLength(128);
        }
    }
}