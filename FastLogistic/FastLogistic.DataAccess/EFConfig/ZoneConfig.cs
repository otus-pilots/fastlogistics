﻿using FastLogistic.Core.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FastLogistic.DataAccess.EFConfig
{
    public class ZoneConfig : IEntityTypeConfiguration<Zone>
    {
        public void Configure(EntityTypeBuilder<Zone> builder)
        {
            builder
                .HasKey(zone => zone.Id);

            builder
                .Property(zone => zone.Id)
                .HasIdentityOptions(100);
            builder
                .Property(zone => zone.Name)
                .HasMaxLength(128);
        }
    }
}