﻿using FastLogistic.Core.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FastLogistic.DataAccess.EFConfig
{
    public class OrderConfig : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder
                .HasKey(o => o.Id);

            builder
                .HasMany(o => o.Statuses)
                .WithMany(s => s.Orders)
                .UsingEntity<OrderStatus>(
                    j => j
                        .HasOne(pt => pt.Status)
                        .WithMany(t => t.OrderStatuses)
                        .HasForeignKey(pt => pt.StatusId),
                    j => j
                        .HasOne(pt => pt.Order)
                        .WithMany(p => p.OrderStatuses)
                        .HasForeignKey(pt => pt.OrderId),
                    j =>
                    {
                        j.Property(pt => pt.TimeChanged).HasDefaultValueSql("CURRENT_TIMESTAMP");
                        j.HasKey(t => new
                        {
                            t.OrderId,
                            t.StatusId,
                            t.TimeChanged
                        });
                    });

            builder.HasMany(o => o.Users)
                .WithMany(s => s.Orders)
                .UsingEntity<UserOrders>(
                    j => j
                        .HasOne(pt => pt.User)
                        .WithMany(t => t.UserOrders)
                        .HasForeignKey(pt => pt.UserId),
                    j => j
                        .HasOne(pt => pt.Order)
                        .WithMany(p => p.UserOrders)
                        .HasForeignKey(pt => pt.OrderId),
                    j =>
                    {
                        j.HasKey(t => new { t.UserId, t.OrderId, t.IsActive });
                    });


            builder
                .HasOne(o => o.Seller)
                .WithMany(s => s.Orders);

            builder
                .HasOne(o => o.AddressFrom)
                .WithMany()
                .HasForeignKey(o => o.AddressFromId);

            builder
                .HasOne(o => o.AddressTo)
                .WithMany()
                .HasForeignKey(o => o.AddressToId);

            builder
                .HasOne(o => o.Courier)
                .WithMany(c => c.Orders);
        }
    }
}