using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace FastLogistic.DataAccess.Data
{
    public static class DbInitializer
    {
        public static void Initialize(DataContext context)
        {
            context.Database.Migrate();

            if (context.Roles.Any())
                return;

            context.Roles.AddRange(FakeDataFactory.Roles);
            context.Statuses.AddRange(FakeDataFactory.Statuses);
            context.Zones.AddRange(FakeDataFactory.Zones);
            context.Addresses.AddRange(FakeDataFactory.Addresses);
            context.Users.AddRange(FakeDataFactory.Users);
            context.Sellers.AddRange(FakeDataFactory.Sellers);
            context.Couriers.AddRange(FakeDataFactory.Couriers);

            context.SaveChanges();
        }
    }
}