using System.Collections.Generic;
using System.Linq;
using FastLogistic.Core.Domain;

namespace FastLogistic.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Address> Addresses => new List<Address>
        {
            new()
            {
                Id = 1,
                RegistrationAddress = "ул. Центральная, 10",
                ZoneId = Zones.Single(zone => zone.Name == "Город").Id
            },
            new()
            {
                Id = 2,
                RegistrationAddress = "ул. Восточная, 3",
                ZoneId = Zones.Single(zone => zone.Name == "Город").Id
            },
            new()
            {
                Id = 3,
                RegistrationAddress = "ул. Пригородная, 1",
                ZoneId = Zones.Single(zone => zone.Name == "Пригород").Id
            },
            new()
            {
                Id = 4,
                RegistrationAddress = "ул. Западная, 3",
                ZoneId = Zones.Single(zone => zone.Name == "Город").Id
            },
            new()
            {
                Id = 5,
                RegistrationAddress = "ул. Дальняя, 18",
                ZoneId = Zones.Single(zone => zone.Name == "Область").Id
            }
        };

        public static IEnumerable<Zone> Zones => new List<Zone>
        {
            new()
            {
                Id = 1,
                Name = "Город",
                CostRate = 1,
                TimeRate = 1
            },
            new()
            {
                Id = 2,
                Name = "Пригород",
                CostRate = 1.25,
                TimeRate = 1.5
            },
            new()
            {
                Id = 3,
                Name = "Область",
                CostRate = 2.0,
                TimeRate = 2.5
            }
        };

        public static IEnumerable<Role> Roles => new List<Role>
        {
            new()
            {
                Id = (int)RoleStatics.Guest,
                Name = RoleStatics.Guest.ToString()
            },
            new()
            {
                Id = (int)RoleStatics.Registered,
                Name = RoleStatics.Registered.ToString()
            },
            new()
            {
                Id = (int)RoleStatics.Manager,
                Name = RoleStatics.Manager.ToString()
            },
            new()
            {
                Id = (int)RoleStatics.Admin,
                Name = RoleStatics.Admin.ToString()
            },
            new()
            {
                Id = (int)RoleStatics.Seller,
                Name = RoleStatics.Seller.ToString()
            },
            new()
            {
                Id = (int)RoleStatics.Courier,
                Name = RoleStatics.Courier.ToString()
            }
        };

        public static IEnumerable<Status> Statuses => new List<Status>
        {
            new()
            {
                Id = 1,
                Name = "Draft"
            },
            new()
            {
                Id = 2,
                Name = "New"
            },
            new()
            {
                Id = 3,
                Name = "InWork"
            },
            new()
            {
                Id = 4,
                Name = "Accepted"
            },
            new()
            {
                Id = 5,
                Name = "Delivering"
            },
            new()
            {
                Id = 6,
                Name = "Delivered"
            },
            new()
            {
                Id = 7,
                Name = "Cancelled"
            },
            new()
            {
                Id = 8,
                Name = "Returning"
            },
            new()
            {
                Id = 9,
                Name = "Returned"
            }
        };

        public static IEnumerable<User> Users => new List<User>
        {
            new()
            {
                Id = 1,
                Email = "admin@mail.com",
                Login = "admin",
                Password = "password",
                PhoneNumber = "",
                RoleId = Roles.Single(role => role.Name == "Admin").Id
            },
            new()
            {
                Id = 2,
                Email = "manager1@mail.com",
                Login = "manager1",
                Password = "password",
                PhoneNumber = "",
                RoleId = Roles.Single(role => role.Name == "Seller").Id
            },
            new()
            {
                Id = 3,
                Email = "manager2@mail.com",
                Login = "manager2",
                Password = "password",
                PhoneNumber = "",
                RoleId = Roles.Single(role => role.Name == "Courier").Id
            },
            new()
            {
                Id = 4,
                Email = "manager3@mail.com",
                Login = "manager3",
                Password = "password",
                PhoneNumber = "",
                RoleId = Roles.Single(role => role.Name == "Manager").Id
            }
        };

        public static IEnumerable<Seller> Sellers => new List<Seller>
        {
            new()
            {
                Id = 1,
                Name = "Seller 1",
                UserId = Users.Single(user => user.Email == "manager1@mail.com").Id
            }
        };

        public static IEnumerable<Courier> Couriers => new List<Courier>
        {
            new()
            {
                Id = 1,
                UserId = Users.Single(user => user.Email == "manager2@mail.com").Id,
                BaseRate = 300,
                BaseTime = 6,
                MaxWeight = 20,
                WeightRate = 1,
                CourierZones = new List<CourierZone>
                {
                    new()
                    {
                        CourierId = 1,
                        ZoneId = Zones.Single(zone => zone.Name == "Город").Id
                    },
                    new()
                    {
                        CourierId = 1,
                        ZoneId = Zones.Single(zone => zone.Name == "Пригород").Id
                    }
                }
            }
        };
    }
}
