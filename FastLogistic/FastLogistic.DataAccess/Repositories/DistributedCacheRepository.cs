﻿using FastLogistic.Core.Abstractions.Repositories;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Text;
using System.Threading.Tasks;

namespace FastLogistic.DataAccess.Repositories
{
    public class DistributedCacheRepository : IDistributedCacheRepository
    {
        private readonly IDistributedCache _distributedCache;
        private readonly ILogger<DistributedCacheRepository> _logger;

        public DistributedCacheRepository(
            IDistributedCache distributedCache,
            ILogger<DistributedCacheRepository> logger
            )
        {
            _distributedCache = distributedCache;
            _logger = logger;
        }

        ///<inheritdoc/>
        public async Task<TV> GetCache<TV>(string key)  where TV : class
        {
            var cache = await _distributedCache.GetAsync(key);
            if (cache == null)
                return null;

            _logger.LogInformation($"Получаем {key} из кэша");
            var serializedList = Encoding.UTF8.GetString(cache);
            var list = JsonConvert.DeserializeObject<TV>(serializedList);
            return list;
        }

        ///<inheritdoc/>
        public async Task SetCache<TV>(string key, TV item) where TV : class
        {
            var serialized = JsonConvert.SerializeObject(item,
                                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
            var cached = Encoding.UTF8.GetBytes(serialized);
            var options = new DistributedCacheEntryOptions()
                .SetAbsoluteExpiration(DateTimeOffset.Now.AddSeconds(15));
            await _distributedCache.SetAsync(key, cached, options);
        }
    }
}
