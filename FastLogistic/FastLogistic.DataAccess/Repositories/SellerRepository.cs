using System.Threading.Tasks;
using FastLogistic.Core.Abstractions.Repositories;
using FastLogistic.Core.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace FastLogistic.DataAccess.Repositories
{
    public class SellerRepository : EfRepository<Seller>, ISellerRepository
    {
        public SellerRepository(            
            DataContext dataContext,
            ILogger<EfRepository<Seller>> logger,
            IDistributedCacheRepository distributedCacheRepository)
            : base(dataContext, logger, distributedCacheRepository)
        {
        }

        public override async Task<Seller> GetByIdAsync(int id)
        {
            return await DbContext.Sellers
                .Include(c => c.Orders)
                .Include(c => c.User)
                .FirstOrDefaultAsync(c => c.Id == id);
        }

        public override async Task<int> AddAsync(Seller entity)
        {
            var user = await DbContext.Users.FirstOrDefaultAsync(u => u.Id == entity.UserId);
            user.RoleId = 3;

            var result = await DbContext.Set<Seller>().AddAsync(entity);
            await DbContext.SaveChangesAsync();
            return result.Entity.Id;
        }

        public override async Task DeleteAsync(Seller seller)
        {
            var user = await DbContext.Users.FirstOrDefaultAsync(u => u.Id == seller.UserId);
            user.RoleId = 2;
            DbContext.Set<User>().Update(user);

            seller.UserId = null;
            DbContext.Set<Seller>().Update(seller);

            await DbContext.SaveChangesAsync();
        }
    }
}