using System;
using System.Threading.Tasks;
using FastLogistic.Core.Abstractions.Repositories;
using FastLogistic.Core.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace FastLogistic.DataAccess.Repositories
{
    public class UserRepository : EfRepository<User>, IUserRepository
    {
        public UserRepository(
            DataContext dataContext,
            ILogger<EfRepository<User>> logger,
            IDistributedCacheRepository distributedCacheRepository)
            : base(dataContext, logger, distributedCacheRepository)
        {
        }

        public override async Task<User> GetByIdAsync(int id)
        {
            return await DbContext.Users
                .Include(u => u.Role)
                .Include(u => u.Seller)
                .Include(u => u.Courier)
                .FirstOrDefaultAsync(u => u.Id == id);
        }

        public async Task<User> GetUserOrdersByIdAsync(int id)
        {
            return await DbContext.Users
                .Include(u => u.Orders)
                .FirstOrDefaultAsync(u => u.Id == id);
        }

        public async Task<User> GetUserCredByLoginAsync(String login)
        {
            return await DbContext.Users
                .FirstOrDefaultAsync(u => u.Login == login);
        }
        public async Task<User> GetUserByLoginAsync(String login)
        {
            return await DbContext.Users
                .Include(u => u.Role)
                .Include(u => u.Seller)
                .Include(u => u.Courier)
                .FirstOrDefaultAsync(u => u.Login == login);
        }

        public async Task<User> GetUserByEmailAsync(String email)
        {
            return await DbContext.Users
                .Include(u => u.Role)
                .Include(u => u.Seller)
                .Include(u => u.Courier)
                .FirstOrDefaultAsync(u => u.Email == email);
        }
    }
}