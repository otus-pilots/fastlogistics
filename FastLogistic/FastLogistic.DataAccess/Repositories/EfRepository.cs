﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FastLogistic.Core;
using FastLogistic.Core.Abstractions.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace FastLogistic.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T>
        where T : BaseEntity
    {
        protected readonly DataContext DbContext;
        protected readonly IDistributedCacheRepository DistributedCacheRepository;
        protected readonly ILogger<EfRepository<T>> Logger;

        public EfRepository(
            DataContext dataContext,
            ILogger<EfRepository<T>> logger, 
            IDistributedCacheRepository distributedCacheRepository
            )
        {
            DbContext = dataContext;
            Logger = logger;
            DistributedCacheRepository = distributedCacheRepository;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var key = $"{typeof(T).Name}/All";

            var list = await DistributedCacheRepository.GetCache<List<T>>(key);

            if (list != null)
                return list;

            Logger.LogInformation($"Получаем {key} из БД");
            list = await DbContext.Set<T>().ToListAsync();

            await DistributedCacheRepository.SetCache(key, list);

            return list;
        }

        public virtual async Task<T> GetByIdAsync(int id)
        {
            // var key = $"{typeof(T).Name}ById/{id}";

            // var item = await DistributedCacheRepository.GetCache<T>(key);
            // if (item != null)
            //     return item;

            // Logger.LogInformation($"Получаем {key} из БД");
            var item = await DbContext.Set<T>().FindAsync(id);

            // await DistributedCacheRepository.SetCache(key, item);

            return item;
        }

        public virtual async Task<int> AddAsync(T entity)
        {
            var result = await DbContext.Set<T>().AddAsync(entity);
            await DbContext.SaveChangesAsync();
            return result.Entity.Id;
        }

        public async Task UpdateAsync(T entity)
        {
            DbContext.Set<T>().Update(entity);
            await DbContext.SaveChangesAsync();
        }

        public virtual async Task DeleteAsync(T entity)
        {
            DbContext.Set<T>().Remove(entity);
            await DbContext.SaveChangesAsync();
        }
    }
}