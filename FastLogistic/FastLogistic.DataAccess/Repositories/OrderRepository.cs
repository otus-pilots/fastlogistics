using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FastLogistic.Core.Abstractions.Repositories;
using FastLogistic.Core.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace FastLogistic.DataAccess.Repositories
{
    public class OrderRepository : EfRepository<Order>, IOrderRepository
    {
        public OrderRepository(
            DataContext dataContext,
            ILogger<EfRepository<Order>> logger,
            IDistributedCacheRepository distributedCacheRepository)
            : base(dataContext, logger, distributedCacheRepository)
        {
        }

        public override async Task<Order> GetByIdAsync(int id)
        {
            // var key = $"OrderById/{id}";
            //
            // var order = await DistributedCacheRepository.GetCache<Order>(key);
            //
            // if (order != null)
            //     return order;

            // Logger.LogInformation($"Получаем {key} из БД");
            var order = await DbContext.Orders
                .Include(o => o.AddressFrom)
                .Include(o => o.AddressTo)
                .Include(o => o.Courier)
                .Include(o => o.Seller)
                .FirstOrDefaultAsync(o => o.Id == id);

            // await DistributedCacheRepository.SetCache(key, order);

            return order;
        }

        public async Task<List<Order>> GetAllFilteredAsync(OrderFilter filter)
        {
            // var key = new StringBuilder("OrderFiltered");

            var filteredOrders = DbContext.Orders.AsQueryable();

            filteredOrders = filteredOrders
                .Include(order => order.AddressFrom)
                .Include(order => order.AddressTo)
                .Include(order => order.Seller)
                .Include(order => order.Courier);

            if (filter.CourierId.HasValue)
            {
                // key.Append($"/courierId={filter.CourierId.Value}");
                filteredOrders = filteredOrders
                    .Where(order => order.Courier.Id == filter.CourierId.Value);
            }

            if (filter.SellerId.HasValue)
            {
                // key.Append($"/sellerId={filter.SellerId.Value}");
                filteredOrders = filteredOrders
                    .Where(order => order.Seller.Id == filter.SellerId.Value);
            }

            if (filter.AddressFromId.HasValue)
            {
                // key.Append($"/addressFromId={filter.AddressFromId.Value}");
                filteredOrders = filteredOrders
                    .Where(order => order.AddressFromId == filter.AddressFromId.Value);
            }

            if (filter.AddressToId.HasValue)
            {
                // key.Append($"/addressToId={filter.AddressToId.Value}");
                filteredOrders = filteredOrders
                    .Where(order => order.AddressToId == filter.AddressToId.Value);
            }

            if (filter.ZoneFromId.HasValue)
            {
                // key.Append($"/zoneFromId={filter.ZoneFromId.Value}");
                filteredOrders = filteredOrders
                    .Include(order => order.AddressFrom)
                    .Where(order => order.AddressFrom.ZoneId == filter.ZoneFromId.Value);
            }

            if (filter.ZoneToId.HasValue)
            {
                // key.Append($"/zoneToId={filter.ZoneToId.Value}");
                filteredOrders = filteredOrders
                    .Include(order => order.AddressTo)
                    .Where(order => order.AddressTo.ZoneId == filter.ZoneToId.Value);
            }

            if (filter.StatusId.HasValue)
            {
                // key.Append($"/statusId={filter.StatusId.Value}");
                filteredOrders = filteredOrders
                    .Include(order => order.OrderStatuses)
                    .Where(order => order.OrderStatuses
                        .OrderByDescending(os => os.TimeChanged)
                        .First().StatusId == filter.StatusId.Value);
            }

            // var ordersFiltered = await DistributedCacheRepository.GetCache<List<Order>>(key.ToString());

            // if (ordersFiltered != null)
            //     return ordersFiltered;

            // Logger.LogInformation($"Получаем {key} из БД");
            var ordersFiltered = await filteredOrders.ToListAsync();

            // await DistributedCacheRepository.SetCache(key.ToString(), ordersFiltered);

            return ordersFiltered;
        }

        public async Task<List<OrderStatus>> GetOrderStatusesByIdAsync(int id)
        {
            // var key = $"OrderStatusesById/{id}";
            //
            // var orderStatuses = await DistributedCacheRepository.GetCache<List<OrderStatus>>(key);
            //
            // if (orderStatuses != null)
            //     return orderStatuses;

            // Logger.LogInformation($"Получаем {key} из БД");
            var orderStatuses = (await DbContext.Orders
                .Include(order => order.OrderStatuses)
                .ThenInclude(orderStatus => orderStatus.Status)
                .FirstOrDefaultAsync(order => order.Id == id)).OrderStatuses;

            // await DistributedCacheRepository.SetCache(key, orderStatuses);

            return orderStatuses;
        }

        ///<inheritdoc/>
        public async Task<Order> GetOrderByTrackAsync(Int64 trackNumber)
        {
            // var key = $"OrderByTrackNumber/{trackNumber}";
            //
            // var order = await DistributedCacheRepository.GetCache<Order>(key);
            //
            // if (order != null)
            //     return order;

            // Logger.LogInformation($"Получаем {key} из БД");
            var order = await DbContext.Orders
                .Include(o => o.AddressFrom)
                .Include(o => o.AddressTo)
                .Include(o => o.Courier)
                .Include(o => o.Seller)
                .FirstOrDefaultAsync(o => o.TrackNumber == trackNumber);

            // await DistributedCacheRepository.SetCache(key, order);

            return order;
        }
    }
}