using System.Threading.Tasks;
using FastLogistic.Core.Abstractions.Repositories;
using FastLogistic.Core.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace FastLogistic.DataAccess.Repositories
{
    public class CourierRepository : EfRepository<Courier>, ICourierRepository
    {

        public CourierRepository(
            DataContext dataContext,
            ILogger<EfRepository<Courier>> logger, 
            IDistributedCacheRepository distributedCacheRepository )
            : base(dataContext, logger, distributedCacheRepository)
        {
        }

        public override async Task<Courier> GetByIdAsync(int id)
        {
            return await DbContext.Couriers
                .Include(c => c.Orders)
                .Include(c => c.Zones)
                .Include(c => c.User)
                .FirstOrDefaultAsync(c => c.Id == id);
        }

        public override async Task<int> AddAsync(Courier entity)
        {
            var user = await DbContext.Users.FirstOrDefaultAsync(u => u.Id == entity.UserId);
            user.RoleId = 3;

            var result = await DbContext.Set<Courier>().AddAsync(entity);
            await DbContext.SaveChangesAsync();
            return result.Entity.Id;
        }

        public override async Task DeleteAsync(Courier courier)
        {
            var user = await DbContext.Users.FirstOrDefaultAsync(u => u.Id == courier.UserId);
            user.RoleId = 2;
            DbContext.Set<User>().Update(user);

            courier.UserId = null;
            DbContext.Set<Courier>().Update(courier);
            await DbContext.SaveChangesAsync();
        }

        public async Task<Courier> GetCourierIdByUserIdAsync(int userId)
        {
            return await DbContext.Couriers.FirstOrDefaultAsync(c => c.UserId == userId);
        }
    }
}