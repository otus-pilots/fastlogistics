ARG IMAGE_VERSION=5.0-alpine
FROM mcr.microsoft.com/dotnet/sdk:${IMAGE_VERSION} AS build

ARG I=Infrastructure
WORKDIR /src/${I}
COPY ["${I}/EventBus/EventBusInfrastructure/EventBusInfrastructure.csproj", "EventBus/EventBusInfrastructure/"]
COPY ["${I}/EventBus/EventBusRabbitMQ/EventBusRabbitMQ.csproj", "EventBus/EventBusRabbitMQ/"]
COPY ["${I}/IntegrationEvents/BusinesEvents/BusinesEvents.csproj", "IntegrationEvents/BusinesEvents/"]

ARG M=Microservices
ARG N=Notifier
WORKDIR /src/${M}/${N}
COPY ["${M}/${N}/${N}.Core/${N}.Core.csproj", "${N}.Core/"]
COPY ["${M}/${N}/${N}.DataAccess/${N}.DataAccess.csproj", "${N}.DataAccess/"]
COPY ["${M}/${N}/${N}.Service/${N}.Service.csproj", "${N}.Service/"]
RUN dotnet restore "${N}.Service/${N}.Service.csproj"

WORKDIR /src
COPY ${I} ${I}
COPY ${M}/${N} ${M}/${N}

WORKDIR /src/${M}/${N}/${N}.Service
RUN dotnet publish . -c Debug -o /app

FROM mcr.microsoft.com/dotnet/aspnet:${IMAGE_VERSION} AS runtime
WORKDIR /app
EXPOSE 80
COPY --from=build /app .

ENTRYPOINT [ "dotnet", "Notifier.Service.dll" ]