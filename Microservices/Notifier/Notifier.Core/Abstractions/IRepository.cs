using System.Threading.Tasks;

namespace Notifier.Core.Abstractions
{
    public interface IRepository<T>
    {
        Task AddAsync(T entity);
    }
}