using System;

namespace Notifier.Core.Domain
{
    public class EmailNotificationEvent
    {
        public Guid Id { get; set; }
        public DateTime DateTime { get; set; }
        public string Email { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
    }
}