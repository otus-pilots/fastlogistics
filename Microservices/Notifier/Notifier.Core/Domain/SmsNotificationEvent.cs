using System;

namespace Notifier.Core.Domain
{
    public class SmsNotificationEvent
    {
        public Guid Id { get; set; }
        public DateTime DateTime { get; set; }
        public string Phone { get; set; }
        public string Message { get; set; }
    }
}