using MongoDB.Driver;

namespace Notifier.DataAccess
{
    public interface IDataContext
    {
        IMongoDatabase Database { get; set; }
    }
}