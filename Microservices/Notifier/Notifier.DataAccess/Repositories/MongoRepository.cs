using System.Threading.Tasks;
using MongoDB.Driver;
using Notifier.Core.Abstractions;

namespace Notifier.DataAccess.Repositories
{
    public class MongoRepository<T> : IRepository<T>
    {
        private readonly IMongoCollection<T> _collection;

        public MongoRepository(IDataContext context)
        {
            _collection = context.Database.GetCollection<T>(typeof(T).Name);
        }

        public async Task AddAsync(T entity)
        {
            await _collection.InsertOneAsync(entity);
        }
    }
}