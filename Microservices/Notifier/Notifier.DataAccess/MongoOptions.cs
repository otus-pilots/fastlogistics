using Notifier.Core.Abstractions;

namespace Notifier.DataAccess
{
    public class MongoOptions : IMongoOptions
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}