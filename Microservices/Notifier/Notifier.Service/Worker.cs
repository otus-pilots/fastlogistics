using System.Threading;
using System.Threading.Tasks;
using BusinesEvents;
using EventBusInfrastructure.Abstraction;
using Microsoft.Extensions.Hosting;
using Notifier.Service.EventHandlers;

namespace Notifier.Service
{
    public class Worker : BackgroundService
    {
        private IEventBus EventBus { get; }

        public Worker(IEventBus eventBus)
        {
            EventBus = eventBus;
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            EventBus.Subscribe<OrderStatusEmailNotifEvent, EmailNotificationEventHandler>();
            EventBus.Subscribe<OrderStatusSMSNotifEvent, SmsNotificationEventHandler>();

            EventBus.StartConsume();
            
            return base.StartAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await Task.CompletedTask;
            }
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            EventBus.Unsubscribe<OrderStatusEmailNotifEvent, EmailNotificationEventHandler>();
            EventBus.Unsubscribe<OrderStatusSMSNotifEvent, SmsNotificationEventHandler>();

            return base.StopAsync(cancellationToken);
        }
    }
}