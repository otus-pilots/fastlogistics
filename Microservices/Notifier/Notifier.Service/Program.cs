using Autofac.Extensions.DependencyInjection;
using EventBusRabbitMQ;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Notifier.Core.Abstractions;
using Notifier.DataAccess;
using Notifier.DataAccess.Repositories;
using Notifier.Service.EventHandlers;

namespace Notifier.Service
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        private static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureServices(ConfigureServices)
                .UseWindowsService()
                .UseSystemd();
        }

        private static void ConfigureServices(HostBuilderContext context, IServiceCollection services)
        {
            var configuration = context.Configuration;

            services.Configure<MongoOptions>(configuration.GetSection(nameof(MongoOptions)));
            services.AddSingleton<IMongoOptions>(x => x.GetRequiredService<IOptions<MongoOptions>>().Value);

            services.AddScoped<IDataContext, DataContext>();
            services.AddScoped(typeof(IRepository<>), typeof(MongoRepository<>));

            services.AddEventBus(configuration);
            services.AddTransient<EmailNotificationEventHandler>();
            services.AddTransient<SmsNotificationEventHandler>();

            services.AddHostedService<Worker>();
        }
    }
}