using System;
using System.Threading.Tasks;
using BusinesEvents;
using EventBusInfrastructure.Abstraction;
using Notifier.Core.Abstractions;
using Notifier.Core.Domain;

namespace Notifier.Service.EventHandlers
{
    public class SmsNotificationEventHandler : IIntegrationEventHandler<UserSMSNotifEvent>
    {
        private readonly IRepository<SmsNotificationEvent> _smsNotificationEventRepository;

        public SmsNotificationEventHandler(IRepository<SmsNotificationEvent> smsNotificationEventRepository)
        {
            _smsNotificationEventRepository = smsNotificationEventRepository;
        }

        private async Task SendSmsAsync(string phoneNumber, string message)
        {
            // Здесь отправляем СМС

            var notification = new SmsNotificationEvent()
            {
                Id = Guid.NewGuid(),
                DateTime = DateTime.Now,
                Phone = phoneNumber,
                Message = message
            };

            await _smsNotificationEventRepository.AddAsync(notification);
        }

        public async Task Handle(UserSMSNotifEvent @event)
        {
            await SendSmsAsync(@event.PhoneNumber, @event.Body);
        }
    }
}