using System;
using System.Threading.Tasks;
using BusinesEvents;
using EventBusInfrastructure.Abstraction;
using Notifier.Core.Abstractions;
using Notifier.Core.Domain;

namespace Notifier.Service.EventHandlers
{
    public class EmailNotificationEventHandler : IIntegrationEventHandler<UserEmailNotifEvent>
    {
        private readonly IRepository<EmailNotificationEvent> _emailNotificationEventRepository;

        public EmailNotificationEventHandler(IRepository<EmailNotificationEvent> emailNotificationEventRepository)
        {
            _emailNotificationEventRepository = emailNotificationEventRepository;
        }

        private async Task SendEmailAsync(string email, string title, string message, DateTime dateTime)
        {
            // Здесь отправляем сообщение

            var notification = new EmailNotificationEvent()
            {
                Id = Guid.NewGuid(),
                DateTime = DateTime.Now,
                Email = email,
                Title = title,
                Message = message
            };

            await _emailNotificationEventRepository.AddAsync(notification);
        }

        public async Task Handle(UserEmailNotifEvent @event)
        {
            await SendEmailAsync(@event.Email, @event.Title, @event.Body, @event.CreationDate);
        }
    }
}